package emcorp.studio.gameborneo.Library;

/**
 * Created by ASUS on 31/05/2016.
 *
 * sesuaikan index dari tiap2 array sesuai dengan urutan dari vaariable kota
 *
 */
public class Constant {
    //Format Array Pertanyaan --> Pertanyaan, opsi 1, opsi 2, opsi 3, opsi 4, jawaban
    //Format Array Informasi --> Background, informasi, music, gambar 1, info 1, gambar 2, info 2, gambar 3, info 3
    //PONTIANAK

    public static final int PONTIANAK = 0;
    public static final int KUBU_RAYA = 1;
    public static final int MEMPAWAH = 2;
    public static final int SINGKAWANG = 3;
    public static final int SAMBAS = 4;
    public static final int BENGKAYANG = 5;
    public static final int LANDAK = 6;
    public static final int SANGGAU = 7;
    public static final int SEKADAU = 8;
    public static final int MELAWI = 9;
    public static final int SINTANG = 10;
    public static final int KAPUAS_HULU = 11;
    public static final int KETAPANG = 12;
    public static final int KAYONG_UTARA = 13;

    public static final String[] Label_kota = new String[]{
            "Pontianak",
            "Kubu Raya",
            "Mempawah",
            "Singkawang",
            "Sambas",
            "Bengkayang",
            "Landak",
            "Sanggau",
            "Sekadau",
            "Melawi",
            "Sintang",
            "Kapuas Hulu",
            "Ketapang",
            "Kayong Utara",
    };


    public static final int ROUTE_1 = 0;
    public static final int ROUTE_2 = 1;
    public static final int ROUTE_3 = 2;
    public static final int ROUTE_4 = 3;
      

    private String Question[][] = new String[][]{
        // index 0 => Kota pontianak
        {
            "Nama ibu kota Provinsi Kalimantan Barat adalah....",
            "Tugu khatulistiwa Pontianak didirikan pada tahun....",
            "Garis khatulistiwa membentang melingkari tengah-tengah bumi ",
            "Taman rekreasi tugu khatulistiwa dikelola oleh pemerintah kota Pontianak dalam bentuk UPT. Kepanjangan dari UPT adalah....",
            "Sungai yang terkenal di Pontianak sebagai salah satu jalur tranportasi adalah....",
            "Di mana museum Kal-Bar didirikan?",
            "Masjid yang dibangun bersamaan dengan dibangunnya keraton dan berada di sekitar lingkungan kerajaan adalah....",
            "Yang  menjadi ciri khas dari sotong pangkong adalah  karena proses di-pangkong setelah pemanggangan yang artinya....",
            "Bingke berendam memiliki tekstur yang lembek dan basah karena melalui proses....",

        },

        //index 1 => Kota Kubu Raya
        {
            "Kabupaten Kubu Raya merupakan kabupaten yang terletak di bagian … Provinsi Kalimantan Barat.",
            "Taman fantasia dirancang khusus dengan perpaduan alam terbuka dan berbagai permainan yang bermanfaat bagi anak untuk....",
            "Daya tarik wisata yang sangat menarik untuk dikunjungi di daerah sungai kakap adalah...",
            "Nama lain dari pekong laut yang terletak di kecamatan Sungai Kakap adalah....",
            "Makanan khas dari Kabupaten Kubu Raya adalah....",
        },

        // index 2 => Kota Mempawah
        {
            "Secara geografis, kabupaten Mempawah yang berada di selatan Provinsi Kalimantan Barat berbatasan dengan....",
            "Objek wisata unggulan kabupaten Mempawah yang memiliki pesona alam yang indah dan berhadapan langsung dengan laut natuna, pulau penibung dan pulau temajo adalah....",
            "Pulau temajo terletak langsung dengan….",
            "Kenapa pantai dengan pasir putih yang berada di sungai kunyit kabupaten Mempawah dinamakan pantai kijing?",
            "Olahan beras ketan yang diisi ebi lalu dibungkus dengan daun pisang membentuk kerucut kemudian dibakar merupakan makanan khas Mempawah bernama....",
            "Patlau biasa dibungkus dengan daun pisang dan berbentuk....",
        },

        //index 3 => Kota Singkawang
        {
            "Kata lain dari kota Singkawang yang artinya kota yang terletak di antara laut, muara, gunung, dan sungai adalah....",
            "Masyarakat kota Singkawang yang multi etnis terdiri dari 3 etnis terbesar yakni....",
            "Kepanjangan dari  TPPI adalah....",
            "Taman rekreasi bukit bougenvil terletak di wilayah perbukitan....",
            "Berapa jarak Rindu Alam dengan Pasir Panjang dan Palm Beach?",
            "Yang membedakan rujak ebi singkawang dengan rujak kebanyakan adalah pada taburan ... di atasnya.",
            "Bubur gunting terbuat dari ... yang kemudian dipotong dengan cara digunting-gunting.",
            "Yang menjadi ciri khas dari mie tiauw asu adalah....",

        },

        // index 4 => Kota Sambas
        {
            "Sambas merupakan sebuah kabupaten di Kalimantan Barat yang memiliki .... terpanjang sekitar 128,5 km.",
            "Nama asli dari Sultan Muhammad Tajuddin adalah....",
            "Istana Alwatzikhoebillah pertama kali didirikan oleh..",
            "Sejarah peninggalan jaman penjajahan yang berada di pantai tanjung batu adalah....",
            "Pantai yang terletak 47 km dari ibu kota kabupaten Sambas adalah....",
            "Di mana letak desa Temajuk?",
            "Bubur pedas merupakan cerminan budaya yang kental di kerajaan....",
        },

        //index 5 => Kota Bengkayang
        {
            "Kabupaten Bengkayang merupakan salah satu kabupaten di Kalimantan Barat yang berbatasan langsung dengan negara....",
            "Pulau Randayan biasa dilewati saat menggunakan kapal dari objek wisata....",
            "Berikut adalah kegiatan yang dapat dilakukan di pulau Randayan, kecuali....",
            "Pulau Kabung merupakan salah satu pulau yang terdapat di pesisir....",
            "Berapa luas kawasan pulau kabung?",
            "Berapa ketinggian dan lebar air terjun riam merasap?",
            "Di manakah letak air terjun riam merasap?",
            "Lempok adalah makanan khas sejenis dodol tetapi tidak menggunakan bahan campuran tepung melainkan hanya menggunakan bahan dasar....",
        },

        // index 6 => Kota Landak
        {
            "Berapa jumlah kecamatan yang ada di kabupaten Landak?",
            "Rumah Panjang merupakan identitas etnis Dayak, di manakah letak rumah Panjang yang ada di kabupaten Landak tersebut?",
            "Kompleks istana kerajaan Landak memiliki tiga komponen utama yaitu sebagai berikut, kecuali....",
            "Riam Bananggar terletak cukup jauh dari Kota Pontianak dan mempunyai jalan yang cukup sulit untuk dilewati, yaitu terletak di dusun....",
            "Air terjun Manggar merupakan patahan sungai landak yang mempunyai hutan yang masih alami dengan bermacam varietas flora seperti....",
            "Rotikap dibuat dengan bahan utamanya buah kelapa yaitu dari bagian....",

        },

        //index 7 => Sanggau
        {
            "Pada umumnya wilayah kabupaten Sanggau merupakan daerah dataran tinggi berbukit dan berrawa-rawa yang dialiri oleh beberapa sungai yaitu...",
            "Daya tarik wisata pancur aji merupakan kawasan wisata yang di kelola oleh....",
            "Daya tarik utama di kawasan Sanggau adalah berupa air terjun yaitu....",
            "Di lokasi pancur aji banyak terdapat tanaman langka yang dilindungi seperti hutan tengkawang, albasi dan....",
            "Ai’ Sipatn Lotup artinya....",
            "Sumber air panas Sipatn Lotup berlokasi di desa Sape, kecamatan Jangkang kabupaten Sanggau dengan jarak sekitar.... dari Kabupaten Sanggau.",
            "Kerajaan Sanggau merupakan salah satu kerajaan tertua di Indonesia yang didirikan oleh Putri Daranante dan Babai Cinga pada abad ke....",
            "Salah satu peninggalan kerajaan Sanggau yang masih berdiri kokoh sampai saat ini adalah....",
            "Sungkui dibungkus dengan daun ... atau yang juga sering disebut daun sungkui dan daun sanggau sehingga memiliki rasa dan aroma yang khas.",
            "Kote merupakan kue tradisional Sanggau sejenis pastel yang di dalamnya berisi....",
        },

        // index 8 => Kota Sekadau
        {
            "Kabupaten Sekadau merupakan pemekaran dari Kabupaten...",
            "Batu Bertulis yang terletak di kabupaten Sekadau berukuran tinggi....",
            "Pada Batu Bertulis terdapat tulisan secara vertikal yang menggunakan huruf pallawa berbahasa....",
            "Goa Lawang Kuari terletak di sungai kapuas yang mempunyai 3 buah lubang dengan mulut goa yang mempunyai ukuran...",
            "Air terjun Gurong Sumpit memiliki ketinggian dan lebar....",
            "Bagaimana keadaan udara di air terjun Gurong Sumpit?",
            "Lemang merupakan makanan yang berbahan dasar dari....",
            "Koe lulun merupakan makanan sejenis lepat yang isinya....",
        },

        //index 9 => Kota Melawi
        {
            "Kabupaten Melawi merupakan pemekaran dari kabupaten....",
            "Kabupaten Melawi dialiri oleh sungai yang cukup besar yaitu....",
            "Taman Nasional Bukit Baka - Bukit Raya merupakan kawasan taman nasional terbesar kedua yang ada di Kalimantan Barat setelah taman nasional.....",
            "Beragam flora dan fauna juga terdapat di Taman Nasional Bukit Baka - Bukit Raya seperti....",
            "Taman Nasional Bukit Baka - Bukit raya mempunyai hutan dengan luas....",
            "Menurut cerita penduduk setempat, danau lintah dulunya merupakan sebuah sungai, sungai tersebut menyempit dikarenakan adanya....",
            "Danau lintah digunakan oleh penduduk setempat untuk melakukan hal-hal berikut, kecuali....",
            "Guongk nobongk merupakan air terjun bertingkat 7, di antaranya 2 tingkat mempunyai ketinggian sekitar....",
            "Bahan utama dari oncau ikan adalah ikan dan buah....",
            "Sibung yang dapat diolah menjadi makanan adalah sibung yang....",
            
        },

        // index 10 => Kota Sintang
        {
            "Kabupaten Sintang merupakan sebuah kabupaten yang terkenal dengan bukit batu terbesar kedua di dunia setelah Ayers Rock di Australia, bukit ini bernama bukit....",
            "Berapa luas wilayah Kabupaten Sintang?",
            "Taman wisata bukit kelam ini terletak antara 2 bantaran sungai yaitu....",
            "Di bukit kelam juga terdapat tumbuh-tumbuhan langka yang biasa digunakan oleh masyarakat setempat untuk memasak nasi, tumbuhan itu bernama....",
            "Air terjun nokan nayan berasal dari dua kata yaitu nokan dan nayan. Nokan mempunyai arti....",
            "Air terjun nokan nayan terletak pada pertemuan dua aliran sungai yaitu....",
            "Hutan wisata Baning adalah hutan yang sangat alami terletak di tengah-tengah kota Sintang yang mempunyai luas 215 hektar. Hutan ini  termasuk golongan hutan....",
            "Hutan wisata Baning utuh kaya akan keanekaragaman flora dan fauna seperti berikut, kecuali....",
            "Tempoyak adalah makanan khas Sintang yang terbuat dari olahan buah....",
            "Pekasam adalah makanan khas Sintang yang terbuat dari olahan ikan yang ditaburi dengan .... dan disimpan dalam wadah tertutup selama beberapa hari.",


        },

        //index 11 => Kota Kapuas Hulu
        {
            "Kabupaten Kapuas Hulu disebut juga dengan....",
            "Masyarakat kabupaten Kapuas Hulu adalah mayoritas etnis....",
            "Danau sentarum umumnya berbentuk cekungan datar atau lebak lebung yang merupakan daerah hamparan banjir yang dikelilingi oleh jajaran....",
            "Dari Kawasan taman nasional danau sentarum tercatat saat ini terdapat 675 spesies flora yang tergolong dalam 97 familia, danau sentarum mempunyai jenis tumbuhan yang sama dengan amazon yaitu....",
            "Taman nasional betung kerihun adalah kawasan konservasi terbesar di provinsi Kalimantan Barat dengan luas 800.000 hektar atau sekitar....",
            "Taman ini memiliki subdestinasi seperti deestinasi embaloh yang merupakan daerah sebaran habitat satwa kunci yaitu....",
            "Danau empangau adalah sebuah hamparan danau seluas 124 hektar yang terletak di desa....",
            "Danau Empangau ditetapkan sebagai danau lindung melalui....",
            "Nama lain dari kerupuk basah adalah....",
    
        },

        // index 12 => Kota Ketapang
        {
            "Kabupaten Ketapang merupakan salah satu kabupaten yang terluas di Kalimantan Barat yaitu sekitar ... dari luas Provinsi Kalimantan Barat.",
            "Tepat di desa Mulai Kerta kecamatan Benua Kayong kabupaten Ketapang, kurang lebih 4 km dari pusat ibu kota kabupaten terdapat sebuah keraton yang bernama....",
            "Salah satu daya tarik yang ada di keraton Panembahan Raja G.M Saunan adalah peninggalan yang sudah berumur ratusan tahun yaitu....",
            "Salah satu objek wisata pantai yang ada di Ketapang adalah....",
            "Pantai yang berlokasi tidak jauh dari pusat kota ketapang yaitu hanya berjarak 10 km dan dapat ditempuh menggunakan sepeda motor atau mobil selama 10 menit merupakan pantai...",
            "Pantai Air Mati didominasi oleh tanaman mangrove yang digunakan sebagai tempat persinggahan....",
            "Ale-ale adalah sejenis hewan air yang menyerupai....",
            "Amplang khas Ketapang berbahan dasar ikan tenggiri ataupun ikan....",
        },

        //index 13 => Kota Kayong Utara
        {
            "Kabupaten Kayong Utara dengan kecamatan Sukadana sebagai ibu kota kabupaten merupakan salah satu kabupaten....",
            "Kabupaten Kayong Utara merupakan pemekaran dari kabupaten....",
            "Air terjun riam berasap terletak di kaki gunung seberang hulu sungai siduk dusun Pangak Tapang kecamatan Sukadana yang memiliki ketinggian....",
            "Ketinggian air terjun dan kedalaman kolam ini dipengaruhi oleh pasang surut. Fenomena ketika air terjun jatuh ke sungai dan menyebabkan percikan uap air berupa kabut putih tebal menyerupai asap sehingga diberi nama....",
            "Pantai pulau datok merupakan primadona wisatawan karena letak objek wisata ini berdekatan dengan....",
            "Berapa luas lahan pantai pulau datuk?",
            "Yang membedakan ketupat colet dengan ketupat lainnya adalah terletak pada....",
        },

            


    };

    private String Answer[][][]=new String[][][]{
            //index 0 => Kota pontianak
        {
            {"Sintang","Pontianak","Singkawang"},
            {"1928","1930","1938"},
            {"Belahan selatan dan belahan barat ","Belahan utara dan belahan selatan","Belahan timur dan belahan berat"},
            {"Unit Pengolahan Taman","Unit Pariwisata Taman","Unit Pengolahan Teknis"},
            {"Sungai Kakap","Sungai Mahakam","Sungai Kapuas"},
            {"Ketapang","Pontianak","Singkawang"},
            {"Masjid Jami","Masjid Nurul Iman","Masjid Raya Mujahidin"},
            {"Direndam","Dipukul-pukul","Direbus"},
            {"Penyiraman","Pemanggangan","Pengukusan"},
        },
            //index 1 => Kota Kubu Raya
        {
            {"Timur","Utara","Barat"},
            {"Keberanian dan kemandirian anak","Kreativitas dan kecepatan anak","Kebahagian dan keberanian anak"},
            {"Wisata agro dan wisata peternakan","Wisata agro dan wisata budaya","Wisata budaya dan wisata perikanan"},
            {"Pekong terapung","Kelenteng pekong","Kelenteng laut"},
            {"Bika Ambon","Lumpia","Sale Pisang"},
        },
            //index 2 => Kota Mempawah
        {
            {"Kabupaten Sintang","Kabupaten Kubu Raya","Kabupaten Landak"},
            {"Taman fantasia","Tugu khatulistiwa","Wisata nusantara resort"},
            {"Pantai kijing","Laut natuna","Pontianak"},
            {"Karena terdapat spesies cumi-cumi","Karena banyak terdapat binatang kerang laut","Karena berhadapan dengan pulau temajo"},
            {"Capcay","Lemang","Pengkang"},
            {"Segitiga panjang","Jajar Genjang","Lingkaran"}
            
        },
            //index 3 => Kota Singkawang
        {
            {"san kew jong","hakkajong","san khek jong"},
            {"Cina, dayak, melayu","Melayu, cina, batak","Batak, melayu, dayak"},
            {"Taman Pasir Panjang Indonesia","Taman Pasir Pendek Indah","Taman Pasir Panjang Indah"},
            {"Rindu alam","Pangmilang","Temajok"},
            {"10 km","15 km","18 km"},
            {"Ebi parut","Keju parut","Jeruk nipis"},
            {"Bubur","Kerupuk","Roti goreng"},
            {"Perasan jeruk kunci","Taburan keju","Bentuk mie yang pendek"},

        },

            //index 4 => Kota Sambas
        {
            {"Pantai","Sungai","Danau"},
            {"Raden Agung","Raden Bima","Raden Tanggul"},
            {"Sultan Syarif Hamida","Raden Sulaiman","Sultan Muhammad Tajuddin"},
            {"Meriam Peninggalan Belanda","Meriam Segindeh","Persenjataan"},
            {"Pantai Tanjung Batu","Pantai Palm Beach","Rindu Alam"},
            {"Kepala pulau Kalimantan","Ekor pulau Kalimantan","Badan pulau Kalimantan"},
            {"Jawa","Tionghoa","Melayu Deli"},
        },
            //index 5 => Kota Bengkayang
        {
            {"Singapura","Malaysia","Australia"},
            {"Palm Beach","Taman Pasir Panjang Indah","Rindu Alam"},
            {"Memancing","Diving","Membakar lahan"},
            {"Kabupaten Bengkayang","Kabupaten Sambas","Kabupaten Singkawang"},
            {"18,40 ha","19,40 ha","20,50 ha"},
            {"20 meter dan 8 meter","16 meter dan 5 meter","18 meter dan 7 meter"},
            {"Desa Bodok","Desa Beruan","Desa Sahan"},
            {"Pepaya","Labu","Durian"},
        },

            //index 6 => Kota Landak
        {
            {"12 kecamatan","13 kecamatan","14 kecamatan"},
            {"Kampung Saham, Kecamatan Sengah Temila","Kampung Mandor, Kecamatan Mandor","Kampung Senakin, Kecamatan Mandor"},
            {"Istana Ismahayana Landak","Masjid Djami Keraton Landak","Meriam"},
            {"Dusun Perbuah, Desa Merayuh Kecamatan Air Besar","Dusun Pesayur, Desa Merayuh Kecamatan Air Kecil","Dusun Perikan, Desa Merayuh Kecamatan Air Besar"},
            {"Mawar merah","Kantong semar","Bunga rafflesia"},
            {"Santan","Tempurung","Daging"},
        },
            //index 7 => Kota Sanggau
        {
            {"Sungai Kakap","Sungai Kapuas","Sungai Melawi"},
            {"Pemerintahan Daerah Kabupaten Sanggau","Pemerintahan Daerah Kabupaten Landak","Pemerintahan Daerah Kabupaten Sambas"},
            {"Air terjun engkuli dan setapang","Air terjun merasap dan mananggar","Air terjun dait dan berawan"},
            {"Beringin","Pisang","Pekawai"},
            {"Air es","Air mendidih","Air jatuh"},
            {"10 km","30 km","50 km"},
            {"13","14","15"},
            {"Bangunan meriam","Bangunan keraton Suryanegara","Persenjataan"},
            {"Mengkudu","Keririt","Buas-buas"},
            {"Serundeng kelapa","Sambal teri","Keju parut"},

        },

            //index 8 => Kota Sekadau
        {
            {"Sintang","Sanggau","Mempawah"},
            {"3,9 meter","4,9 meter","5,9 meter"},
            {"Jawa","Sanksekerta","Latin"},
            {"Sangat kecil","Besar","Sangat besar"},
            {"10 meter dan 5 meter","7 meter dan 3 meter","5 meter dan 2 meter"},
            {"Panas","Sangat panas","Sangat dingin"},
            {"Tebu","Gandum","Ketan"},
            {"Gula merah","Serundeng kelapa","Sambal teri"},
        },
            //index 9 => Kota Melawi
        {
            {"Sanggau","Sintang","Sekadau"},
            {"Sungai Kapuas","Sungai Sekayam","Sungai Melawi"},
            {"Betung Kerihun","Taman Palung","Danau Sentarum"},
            {"Jerapah","Orang utan","Hutan pinus"},
            {"183.090 hektar","182.125 hektar","181.090 hektar"},
            {"Pembangunan jalan perusahaan","Hutan yang semakin luas","Longsor"},
            {"Mencari ikan","Mandi","Lomba sampan"},
            {"5 meter","6 meter","7 meter"},
            {"Durian","Manggis","Jambu"},
            {"Muda atau kuncup","Tua atau mekar","Busuk"},
        },

            //index 10 => Kota Sintang
        {
            {"Kelam","Jamur","Bawang"},
            {"21.338 km2","21.638 km2","21.938 km2"},
            {"Sungai Kapuas dan sungai Melawi","Sungai Kapuas dan sungai Mali","Sungai Melawi dan sungai Kakap"},
            {"Kantong semar","Eceng gondok","Daun pisang"},
            {"Air terjun","Air laut","Air hujan"},
            {"Aliran Sungai Kapuas dan Melawi","Aliran Sungai Sekayam dan Kapuas","Aliran Sungai Jenggonoi dan Sungai Nayan"},
            {"Tropis","Subtropis","Lindung"},
            {"Kantong semar","Orang utan","Singa"},
            {"Pepaya","Pisang","Durian"},
            {"Gula","Garam","Sirup"},
        },
            //index 11 => Kota Kapuas Hulu
        {
            {"Bumi ujung Kapuas","Bumi tengah Kapuas","Bumi uncak Kapuas"},
            {"Suku dayak dan melayu","Suku batak dan melayu","Suku jawa dan dayak"},
            {"Perumahan","Pegunungan","Persawahan"},
            {"Pohon pungguk","Pohon pinus","Pohon beringin"},
            {"4 % dari luas total daratan provinsi Kalimantan Barat","4.5 % dari luas total daratan provinsi Kalimantan Barat","5.5 % dari luas total daratan provinsi Kalimantan Barat"},
            {"Orang utan","Macan Sumatra","Badak bercula"},
            {"Desa Nanga Empangau","Desa Lanjak","Desa Badau"},
            {"SK Bupati No. 6 tahun 2000","SK Bupati No. 6 tahun 2001","SK Gubernur No. 6 tahun 2001"},
            {"Kornet","Temet","Colet"},

        },

            //index 12 => Kota Ketapang
        {
            {"31,588 km2","31,888 km2","32,588 km2"},
            {"Keraton Kadariah","Keraton Panembahan Kerajaan Matan G.M Saunan","Keraton Sultan Nata"},
            {"Baju adat","Keris","Meriam padam pelita"},
            {"Pantai Tanjung Belandang","Pantai Kijing","Pantai Pasir Panjang"},
            {"Pantai Air Mati","Pantai Air Hidup","Pantai Air Panas"},
            {"Burung","Buaya","Singa"},
            {"Kura-kura","Kerang","Gurita"},
            {"Arwana","Toman","Belida"},
        },
        //index 13 => Kota Kayong utara
        {
            {"Tertua di provinsi Kalimantan Barat","Termuda di provinsi Kalimantan Barat","Terbagus di provinsi Kalimantan Barat"},
            {"Pontianak","Kubu Raya","Ketapang"},
            {"10 meter","15 meter","20 meter"},
            {"Riam berasap","Riam berkabut","Riam tebal"},
            {"50 jenis mamalia dan 240 jenis burung","71 jenis mamalia dan 250 jenis burung","80 jenis mamalia dan 270 jenis burung"},
            {"Taman Nasional Gunung Palung","Taman Nasional Batang Kerihun","Taman Nasional Bukit Baka-Bukit Raya"},
            {"70 hektar","100 hektar","150 hektar"},
            {"Cara memasak","Bahan dasar","Cara penyajian"},
        },
    };

    private String mAnswer[][]=new String[][]{

            //Index 0 kota pontianak
        {
            "Pontianak",
            "1928",
            "Belahan utara dan belahan selatan",
            "Unit Pengolahan Teknis",
            "Sungai Kapuas",
            "Pontianak",
            "Masjid Jami",
            "Dipukul-pukul",
            "Pengukusan",
        },
            //Index 1 Kota kubu raya
        {
            "Barat",
            "Keberanian dan kemandirian anak",
            "Wisata agro dan wisata budaya",
            "Pekong terapung",
            "Sale Pisang",
        },

            //Index 2 kota mempawah
        {
            "Kabupaten Kubu Raya",
            "Wisata nusantara resort",
            "Laut natuna",
            "Karena terdapat spesies cumi-cumi",
            "Pengkang",
            "Segitiga panjang",
            
        },
            //Index 3 Kota Singkawang 
        {
            "san kew jong",
            "Cina, dayak, melayu",
            "Taman Pasir Panjang Indah",
            "Pangmilang",
            "18 km",
            "Ebi parut",
            "Roti goreng",
            "Perasan jeruk kunci",
        },

            //Index 4 kota Sambas
        {
            "Pantai",
            "Raden Bima",
            "Sultan Muhammad Tajuddin",
            "Meriam Peninggalan Belanda",
            "Pantai Tanjung Batu",
            "Ekor pulau Kalimantan",
            "Melayu Deli",
        },
            //Index 5 Kota bengkayang
        {
            "Malaysia",
            "Taman Pasir Panjang Indah",
            "Membakar lahan",
            "Kabupaten Bengkayang",
            "19,40 ha",
            "20 meter dan 8 meter",
            "Desa Sahan",
            "Durian",
        },

            //Index 6 kota landak
        {
            "13 kecamatan",
            "Kampung Saham, Kecamatan Sengah Temila",
            "Meriam",
            "Dusun Perbuah, Desa Merayuh Kecamatan Air Besar",
            "Kantong semar",
            "Santan",

        },
            //Index 7 Kota sanggau
        {
            "Sungai Kapuas",
            "Pemerintahan Daerah Kabupaten Sanggau",
            "Air terjun engkuli dan setapang",
            "Pekawai",
            "Air mendidih",
            "50 km",
            "13",
            "Bangunan keraton Suryanegara",
            "Keririt",
            "Serundeng kelapa",
            
        },

            //Index 8 kota sekadau
        {
            "Sanggau",
            "3,9 meter",
            "Sanksekerta",
            "Sangat kecil",
            "10 meter dan 5 meter",
            "Sangat dingin",
            "Ketan",
            "Gula merah",
        },
            //Index 9 Kota melawi
        {
            "Sintang",
            "Sungai Melawi",
            "Betung Kerihun",
            "Orang utan",
            "181.090 hektar",
            "Pembangunan jalan perusahaan",
            "Mencari ikan",
            "7 meter",
            "Durian",
            "Muda atau kuncup",
        },

            //Index 10 kota Sintang
        {
            "Kelam",
            "21.638 km2",
            "Sungai Kapuas dan sungai Melawi",
            "Kantong semar",
            "Air terjun",
            "Aliran Sungai Jenggonoi dan Sungai Nayan",
            "Tropis",
            "Singa",
            "Durian",
            "Garam",
        },
            //Index 11 Kota kapuas hulu
        {
            "Bumi uncak Kapuas",
            "Suku dayak dan melayu",
            "Pegunungan",
            "Pohon pungguk",
            "5.5 % dari luas total daratan provinsi Kalimantan Barat",
            "Orang utan",
            "Desa Nanga Empangau",
            "SK Bupati No. 6 tahun 2001",
            "Temet",

        },

            //Index 12 kota ketapang
        {
            "31,588 km2",
            "Keraton Panembahan Kerajaan Matan G.M Saunan",
            "Meriam padam pelita",
            "Pantai Tanjung Belandang",
            "Pantai Air Mati",
            "Burung",
            "Kerang",
            "Belida",
        },
            //Index 13 Kota kapuas hulu
        {
            "Termuda di provinsi Kalimantan Barat",
            "Ketapang",
            "20 meter",
            "Riam berasap",
            "71 jenis mamalia dan 250 jenis burung",
            "Taman Nasional Gunung Palung",
            "100 hektar",
            "Cara penyajian",
        },
    };
    /**
     * [x][]
     * x => idkota
     * 0 => nama_gambar
     * 1 => nama tebak gambar
     * 2 => hints
     * **/
    private String tebak_gambar[][][] = new String[][][]{
            //Index 0 Kota Pontianak
            {
                    //Index 0 => gambarname index
                    {"ic_tugu_pontianak", "Tugu Khatulistiwa","Tugu yang ada di Pontianak"},
                    {"pontianak_kapuas", "Kapuas","Sungai terpanjang di pontianak"},
                    {"pontianak_pengkang", "Pengkang","Makanan khas pontianak"},
                    {"pontianak_radakng","Radakng","Sebutan untuk rumah panjang suku Dayak Kanayatn"},
                    {"pontianak_chaikwe","Chai Kwe","Makanan khas pontianak"},
            },
                    //Index 1 Kota Kubu raya
            {
                    {"kuburaya_sale", "Sale Pisang","Makanan khas kubu raya"},
                    {"taman_fantasia","Taman Fantasia","Taman rekreasi Kubu Raya"},
                    {"sungai_kakap","sungai kakap","Sungai di Kubu Raya"},
                    {"pekong_laut","pekong laut","Vihara lepas pantai kubu raya"},
            },

            {
                    //Index 2 Mempawah
                    {"mempawah_pengkang","Pengkang","Makanan khas Mempawah"},
                    {"nusantara_resort","nusantara resort","Resort di Mempawah"},
                    {"pulau_temajo","Pulau Temajo","Pulau di mempawah"},
                    {"pantai_kijing","Pantai Kijing","Pantai terkenal dengan nama cumi"},
                    {"mempawah_patlau","Patlau","makanan khas Mempawah"},
                
            },
            {
                    //Index 3 Singkawang
                    {"singkawang_sape","Sape","Alat musik khas dayak"},
                    {"taman_pasir_panjang","TPPI","Singkatan dari Taman Pasir Panjang Indonesia"},
                    {"bukit_bougenville","Bukit Bougenvil","Taman terkenal di Singkawang"},
                    {"rindu_alam","Rindu Alam","Wisata alam berupa bukit di singkawang"},
                    {"singkawang_rujak_ebi","Rujak Ebi","Rujak khas Singkawang"},
                    {"singkawang_bubur_gunting","Bubur Gunting","Bubur khas Singkawang"},
                    {"singkawang_mie_tiaw","Mie Tiaw Asu","Mie khas Singkawang"},

            },
            {
                    //index 4 Sambas
                    {"sambas_bubur_pedas", "Bubur pedas","Makanan khas Sambas"},
                    {"istana_alwatzikhoebillah","Istana Alwatzikhoebillah","Istana di Sambas"},
                    {"temajuk","Pantai Temajuk","Pantai di Sambas","Nama pantai di kabupaten Sambas"},

            },
            {
                    //Index 4 bengkayang
                    {"bengkayang_lempok","lempok","Makanan khas Bengkayang"},
                  //  {"pulau_randayan","Pulau Randayan","Pulau di bengkayang"},
                    {"pulau_kabung","Pulau kabung","Pulau di Bengkayang"},
                    {"riam_merasap","Riam Merasap","Air Terjun di Bengkayang"},
            
            },
            {
                    //landak
                    {"landak_betang","Rumah Betang","Rumah adat khas Landak"},
                    {"keraton_ismahayana","Keraton Ismahayana","Istana di Kabupaten Landak"},
                    {"riam_banangar","Riam Banangar","Air Terjun di kabupaten Landak"},
                    {"landak_rotikap","Rotikap","Makanan khas kabupaten Landak"},

            },
            {
                    //sanggau
                    {"sanggau_sungkui","Sungkui","Makanan khas Kabupaten Sanggau"},
                    {"pancur_aji","Pancur Aji","Nama air terjun kabupaten Sanggau"},
                    {"keraton_suryanegara","Keraton Suryanegara","Keraton di Kabupaten sanggau"},
                    {"sanggau_kote","Kote","Makanan khas kabupaten Sanggau"},

            },
            {
                    //sekadau
                    {"batu_bertulis","Batu Bertulis","Prasasti batu di Sekadau"},
                    {"lawang_kuari","Goa Lawang Kuari","Goa di kabupaten Sekadau"},
                    {"gurong_sumpit","Gurong Sumpit","Air terjun Sekadau"},
                    {"sekadau_lemang","Lemang","Nasi bakar bambu khas Sekadau"},
                    {"lulun_koe_sekadau","Koe lulun","Kue khas sekadau"},
        
            },
            {
                    //melawi
                    {"melawi_bukit_baka","Bukit Baka","Nama bukit di kabupaten Melawi"},
                    {"danau_lintah","Danau Lintah","Danau terkenal dengan Lintah di Melawi"},
                    {"guong_nobonk","Guongk Nobongk","Ait terjun di kabupaten melawi"},
                    {"sintang_tempoyak","Oncau ikan","Makanan khas melawi"},
                    {"melawi_sibung","Sibung","Umbi khas Melawi"},
                    {"melawi_mandau","Mandau","Senjata khas suku dayak"},

            },
            {
                    //sintang
                    {"sintang_tempoyak_ikan","Tempoyak ikan","Makanan khas Sintang"},
                    {"bukit_kelam","Bukit Kelam","Nama bukit di kabupaten Sintang"},
                    {"nokan_nayan","Nokan Nayan","Nama air terjun di kabupaten Sintang"},
                    {"baning","Hutan Baning","Nama Hutan di kabupaten Sintang"},
                    {"sintang_pekasam","Pekasam","Olahan fermentasi ikan khas Sintang"},
                    {"sintang_semprong","Kue semprong","Kue khas Kabupaten Sintang"},
            },
            {
                    //Kapuas Hulu
                    {"kapuas_hulu_kerupuk_basah","Kerupuk basah","Makanan khas Kapuas Hulu"},
                    {"danau_sentarum","Danau Sentarum","Danau di kapuas Hulu"},
                    {"betung_kerihun","Orang Utan","Satwa endemik kalimantan"},
                    {"danau_empangau","Danau Empangau","Danau di Kapuas Hulu"},
            },
            {
                    //ketapang
                    {"ketapang_keraton_matan","Keraton matan","Keraton di Kabupaten Ketapang"},
                    {"tanjung_belandang","Tanjung Belandang","obyek wisata berupa pantai terletak di Sungai Awan"},
                    {"ketapang_ale","Ale-ale","Makanan khas Kabupaten Ketapang"},
                    {"ketapang_amplang","Amplang","Kerupuk khas Ketapang"},
        
            },
            {
                    //kayong utara
                    {"kayong_utara_ketupat","Ketupat Colet","Makanan khas idul fitri kayong utara"},
                    {"pantai_pulau_datuk","Pulau Datuk","Pulau di Kabupaten Kayong utara"},
                    {"gunung_palung","Gunung Palung","Nama gunung di Kayong Utara"},

            }
    };

 

    //Index 0 => Background page, index 1 => icon thumbnail, index 2 => deskripsi
    private String info_primary[][] = new String[][]{
        //Pontianak
            {"ic_pontianak","pontianak_logo","Kota Pontianak merupakan ibu kota Provinsi Kalimantan Barat, terletak pada ketinggian antara 0,1–1,5 m di atas permukaan laut. Pontianak dipisahkan oleh sungai Kapuas besar, sungai Kapuas kecil dan sungai landak. Disamping itu kota Pontianak dilintasi oleh garis khatulistiwa yaitu pada posisi 0 derajat 02’ 24’’ LU sampai dengan 0 derajat 05’01’’ LS dan 109 derajat 16’25’’BT sanpai dengan 10 derajat 23’ 01’’ BT. Luas wilayah Kota Pontianak sekitar 17,82 km, terbagi atas 6 kecamatan dan 33 kelurahan. Penduduk kota Pontianak terdiri atas suku melayu, tionghoa, dayak, bugis, jawa, madura dan suku lainnya."},
        //Kuburaya
            {"kubu_raya_bg","kubu_raya_logo","Kabupaten Kubu Raya merupakan kabupaten yang terletak di bagian barat Provinsi Kalimantan Barat dengan luas wilayah sekitar 6.985,2 km2. Secara geografis kedudukan Kabupaten Kuburaya berada di antara garis 108 derajat dan 35’-109 derajat dan 5’ bujur timur serta 0 derajat dan 44’ – 1 derajat 01’ Lintang selatan."},
         //Mempawah   
            {"mempawah_bg","mempawah_logo","Kabupaten Mempawah memiliki luas wilayah 1.276,90 km atau 15,45% dari luas Kalimantan Barat dengan jumlah penduduk sekitar 280.436 jiwa. Kabupaten ini terletak di jalur perdagangan antara ibu kota Provinsi Kalimantan Barat Pontianak dengan kabupaten jalur utara. Secara georgrafis Kabupaten Mempawah berbatasan: sebelah Utara dengan Kabupaten Bengkayang, sebelah Selatan dengan Kabupaten Kubu Raya, sebelah Barat dengan Laut Natuna dan sebelah Timur dengan Kabupaten Landak. Secara geografis kabupaten mempawah beriklim tropis dengan temperatur antara 23-32 derajat"},
        //Singkawang
            {"singkawang_bg","singkawang_logo","Kota Singkawang sejak dulu dikenal sebagai daerah tujuan wisata masyarakat Kalimantan Barat, pasalnya kota ini terkenal dengan pantai yang indah. Masyarakat tionghoa dari suku Khek/Hakka mengatakan bahwa kota Singkawang berasal dari kata San Kew Jong yang artinya kota yang terletak diantara laut, muara, gunung, dan sungai. Kota Singkawang memiliki kesejukan dan pesona alam yang indah. Yang lebih menarik lagi adalah berkumpulnya berbagai etnis (Multi Etnis) dalam kehidupan yang rukun dan damai, saling menghargai dan menghormati. Kehidupan yang multi etnis terdri dari 3 etnis terbesar yakni tionghoa (cina), melayu, dan dayak"},
        //Sambas
            {"sambas_bg","sambas_logo","Kabupaten Sambas terletak di bagian paling utara Provinsi Kalimantan Barat dengan luas wilayah 6.395,7 km2 atau 4,36% dari luas wilayah Provinsi Kalimantan Barat, memiliki 19 kecamatan dan 183 desa. Jarak dari ibu kota provinsi sekitar 224 km ke arah utara melalui jalan darat. Kabupaten Sambas mempunyai pantai terpanjang di Kalimantan Barat yaitu sekitar 128,5 km dan panjang perbatasan negara 97 km"},
        //Bengkayang
            {"bengkayang_bg","bengkayang_logo","Untuk menuju Kabupaten Bengkayang dari ibu kota provinsi hanya dapat ditempuh melalui jalan darat yaitu melalui jalur arah utara dan jalur arah timur, arah utara dengan jarak tempuh 4 jam perjalanan sedangkan arah timur dengan jarak tempuh 2 jam perjalanan. Penduduk daerah kabupaten Bengkayang didominasi oleh suku Dayak."},
        //Landak    
            {"landak_bg","landak_logo","Kabupaten Landak yang membawahi sebanyak 13 kecamatan memiliki luas sebesar 9,909,10 km2 atau sekitar 6,75 persen dari luas wilayah Provinsi Kalimantan Barat. Kecamatan Sengah Temila merupakan kecamatan yang paling luas wilayahnya, yaitu sebesar 1.962 km2 kemudian Kecamatan Ngabang dengan luas wilayah 1.148 km2 serta Kecamatan Air Besar 1.361,20 km2. Adapun kecamatan yang paling kecil luas wilayahnya adalah Kecamatan Sompak yang merupakan pecahan dari Kecamatan Mempawah Hulu"},
        //Sanggau
            {"sanggau_bg","sanggau_logo","Wilayah kabupaten sanggau terletak diantara 1 derajat 10 menit lintang utara dan 0 derajat 35 menit Lintang Selatan serta daratan 109 derajat 45 menit dan111 derajat 11 menit Bujur Timur. Luas Wilayah Kabupaten Sanggau 12.857,70 km2 atau sekitar 12,47% dari luas Provinsi Kalimantan Barat, beriklim tropis "},
        //Sekadau
            {"sekadau_bg","sekadau_logo","Kabupaten Sekadau merupakan pemekaran dari Kabupaten Sanggau, hasil pemekaran tersebut secara fisik, letak geografis kabupaten ini memiliki luas 5.444,2 km2 atau 32,49% dari luas kabupaten sanggau (18,302,00 km2), dengan posisi letak pada 0 derajat 36 menit 53 detik Lintang Utara dan 0 derajat 35 menit Lintang Selatan, serta di antara 110 derajat 48 menit 43 detik dan 111 derajat 11 menit bujur timur. "},
       //Melawi
            {"melawi_bg","melawi_logo","Kabupaten Melawi terletak di bagian timur Provinsi Kalimatan Barat atau diantara 00 derajat 07’ Lintang Utara serta 10 derajat 21’ Lintang Selatan dan 111 derajat 07’ Bujur timur serta 112 derajat 27’ Bujur Timur. Sebagian besar wilayah Kabupaten Melawi merupakan wilayah perbukitan dengan luas 8.818,70 km2 atau 82,85% dari luas wilayah kabupaten melawi 10.644 km2. Kabupaten Melawi merupakan pemekaran dari Kabupaten Sintang. Kabupaten ini dialiri oleh sungai yang cukup besar yaitu Sungai Melawi dan Sungai Pinoh."},
       //Sintang
            {"sintang_bg","sintang_logo","Kabupaten Sintang merupakan kabupaten yang terkenal dengan bukit kedua setelah Australia, bukit yang diberi nama Bukit Kelam ini, terletak di sebelah timur Provinsi Kalimantan Barat. Diukur dari letak geografis dan luas wilayah Kabupaten Sintang, terbentang seluas 21.638 km2 yang mana juga menempati posisi strategis karena berbatasan langsung dengan Sarawak (Malaysia Timur) serta Brunei Darussalam, sehingga menjadikannya gerbang terdepan keluar masuk barang dan orang dari dan ke Sarawak maupun Brunei Darussalam melalui jalur Darat."},
        //Kapuas Hulu
            {"kapuas_hulu_bg","kapuas_hulu_logo","Kabupaten Kapuas Hulu dikenal dengan sebutan 'Bumi Uncak Kapuas'. Secara umum kabupaten ini memanjang dari arah Barat ke Timur, dengan jarak tempuh terpanjang kurang lebih 240 km dan melebar dari Utara ke Selatan kurang lebih 126,70 km serta merupakan kabupaten paling timur di Provinsi Kalimantan Barat. Masyarakat kabupaten ini mayoritas berasal dari etnis suku Dayak dan Melayu."},
        //Ketapang    
            {"ketapang_bg","ketapang_logo","Kabupaten Ketapang merupakan salah satu kabupaten yang terluas di Kalimantan Barat yaitu sekitar 31.588 km2 atau 24,3 % dari luas Provinsi Kalimantan Barat (146.807 km2) Jarak dari ibukota provinsi sekitar 350 km ke arah selatan. Wilayah kabupaten ini memiliki potensi daya tarik wisata baik alam, budaya maupun minat khusus. Potensi flora fauna, laut dan pesisir pantai, pulau, hutan, seni kebudayaan, sejarah serta atraksi wisata lainnya masih tetap terjaga kelestariannya dalam keserasian etnis."},
        //Kayong Utara    
            {"kayong_utara_bg","kayong_utara_logo","Kabupaten Kayong Utara dengan Kecamatan Sukadana sebagai ibu kota kabupaten merupakan salah satu kabupaten termuda di Provinsi Kalimatan Barat. Kabupaten ini merupakan kabupaten pemekaran dari Kabupaten Ketapang. Kabupaten ini memiliki luas wilayah kurang lebih 4.221 km2 yang terdiri dari wilayah pantai, pesisir, dan kepulauan dengan jumlah penduduk 90.239 jiwa yang tersebar di lima kecamatan."},

    };
    /**
     * index[x][y][0] => Gambar icon,
     * index[x][y][1] => Deskripsi
     */
    private String[][][] info_secondary = new String[][][]{
        //Pontianak
        {
            {"ic_tamanalun_pontianak","Taman Alun Kapuas teletak tepat di jantung Kota Pontianak di sisi pinggir jalan utama dan arah muka menghadap Sungai Kapuas. Taman ini sangat tepat untuk tempat rekreasi keluarga dan sangat nyaman bila kita ingin menikmati pemandangan Sungai Kapuas yang ramai dengan berbagai aktivitas masyarakat, yang mana sungai merupakan salah satu jalur transportasi masyarakat Kota Pontianak. Taman Alun Kapuas mulai ramai dipadati para pengungjung menjelang sore hingga tengah malam."},
            {"ic_museum_pontianak","Museum Kalbar dibangun di Kota Pontianak pada atahun 1974 diatas tanah seluas 26.167 m2. Museum ini lebih kurang menyimpan 6.276 buah koleksi yang merupakan barang peninggalan tempo dulu, sangat kental dengan khasana budaya daerah Kalimatan Barat. selain menyimpan benda-benda sejarah, tradisi, adat istiadat dan kebudayaan, museum ini juga berperan sebgai media pembinaan pendidikan sejarah, serta sebagai objek wisata unggulan. museum ini terbuka untuk umum dengan waktu kunjungan hari selasa - minggu, buka mulai pukul 08.00 - 14.30"},
            {"ic_tugu_pontianak","Tugu Khatulistiwa di dirikan pertama kali oleh Pemerintahan Belanda pada tahun 1928. Tugu ini merupakan tanda menunjukan bahwa Kota Pontianak di lalui oleh potongan garis lintang 0 derajat garis bujur dimana terbukti pada saat tertentu bayangan pada matahari hilang atau tepat berada di atas kepala. Garis Khatulistiwa membentang melingkari tengah-tengah bumi menjadi 2 belahan yang sama besar yaitu belahan utara dan belahan selatan. Karena letaknya berada di tengah belahan, maka wilayah yang berada di sepanjang garis khatulistiwa mempunyai keistimewaan yang tidak ternilai harganya. Tugu Khatulistiwa teletak pada lokasi strategis dan mudah dijangkau baik melalui jalur darat maupun air. Taman rekreasi ini dikelola oleh Pemerintahan Kota Pontianak dalam bentuk Unit Pengelola Teknis (UPT)"},
            {"sotong_pangkon","Sotong Pangkong adalah sotong atau cumi yang telah dikeringkan dan dimasak dengan cara dipanggang. Proses setelah pemanggangan inilah yang membuatnya khas, yaitu dipangkong atau artinya dipukul-pukul dalam bahasa melayu agar dagingnya terasa lebih empuk. Kemudian disajikan bersama dengan kuah sambal kacang atau asam pedas manis. Namun di beberapa tempat lainnya, sotong tersebut digiling agar lebih mudah mengkonsumsinya."},
            {"bingke","Bingka atau lebih dikenal masyarakat setempat dengan sebutan bingke ini terbuat dari campuran telur, santan, sedikit terigu, dan gula pasir. Untuk jenisnya, bingke dibagi lagi menjadi 2 macam, yaitu bingke biasa dan bingke berendam. Bingke yang biasa memiliki tekstur agak kering (dipanggang) sedangkan bingke berendam melalui proses pengukusan sehingga teksturnya lembek dan basah. Terdapat berbagai varian rasa bingke, diantaranya adalah rasa original, ubi rambat, susu, keju susu, kacang ijo, jagung, labu, coklat dan durian. Kue tradisional ini juga populer di daerah Banjarmasin."},
        },
            //kubu raya
        {
            {"taman_fantasia","Taman Fantasia, Objek wisata ini merupakan arena rekreasi keluarga yang cukup lengkap dan menyenangkan. Taman ini menyediakan berbagai fasilitas dan jauh dari kebisingan hiruk pikuk kota. Letaknya sangat strategis yaitu di Jalan arteri supadio yang menghubungkan antara kota Pontianak dengan bandara supadio. Desain taman ini merupakan perpaduan antara alam terbuka dengan berbagai permainan yang disediakan."},
            {"sungai_kakap","Sungai kakap diambil dari nama salah satu nama dikecamatan di Kabupaten Kubu Raya, lokasinya hanya sekitar 10 km dari ibu kota Provinsi melalui jalan darat atau hanya menghabiskan waktu sekitar 30 menit."},
            {"pekong_laut","Pekong laut ini dibangun pada tahun 1960-an oleh warga keturunan tionghoa yang sudah turun temurun menetap di daerah setempat. Pekong laut ini terletak di Kecamatan Sungai Kakap dimana bangunannya banyak didominasi warna merah serta biru laut sehingga keberadaannya sangat mencolok. Pekong laut ini biasa dinamakan pekong terapung atau kelenteng terapung. Untuk mencapai lokasi dapat ditempuh melalu jalan darat kurang lebih 20 menit dari ibu kota dan dilanjutkan dengan menggunakan motor air kurang lebih 15 menit."},
            {"kubu_raya_sale","Sale pisang merupakan produk pisang yang dibuat dengan proses pengeringan dan pengasapan. Sale dikenal mempunyai rasa dan aroma yang khas."}
        },
            //mempawah
        {
            {"nusantara_resort","Objek wisata nusantara resort merupakan objek wisata unggulan kabupaten Mempawah, dengan luas kurang lebih 5 Ha terletak di Jalan Raya Desa Pasir Kecamatan Mempawah Hilir, wisata ini memiliki pesona alam yang indah dan berhadapan langsung dengan laut natuna, pulau penibung dan pulau temajo serta memiliki fasilitas pendukung seperti hotel dan restaurant. Objek wisata nusantara ini berjarak 73,5 km dari ibu kota Provinsi Kalimantan Barat."},
            {"pulau_temajo","Pulau Temajo, Pulau ini berlokasi di Desa Sungai Kunyit Laut Kecamatan Sungai Kunyit, pulau dengan luas areal 556 Ha (50 km2), berjarak 22 Km dari ibu kota kabupaten, dan 89 Km dari ibu kota Provinsi. Pulau Temajo ini memiliki keindahan alam yang sangat menarik, diperlukan waktu 20 menit untuk mencapai pulau ini dengan menggunakan motor air. Pulau temajo merupakan dataran tinggi dengan hutan tropis yang masih alami dan memiliki hutan bakau dengan panjang lebih kurang 3000 m. Pulau yang terletak langsung dengan laut natuna ini memiliki berbagai macam flora dan fauna seperti anggrek, mangrove, penyu, enggang, lumba-lumba dll."},
            {"pantai_kijing","Pantai Kijing Pantai ini memiliki keunikan pantai dengan pasir putih dan laut yang jernih serta berkarang, disekitar pantai ini terdapat “Kelong” yaitu aktivitas nelayan menangkap jenis spesies ikan dan pada saat malam hari keliatan lampu-lampu minyak para nelayan di laut bagaikan kunang-kunang serta apabila terjadi perubahan musim angin utara, timur dan selatan, di pantai tersebut terdapat jenis spesies seperti cumi-cumi dan ikan-ikan kecil.Pantai ini berlokasi di Desa Sungai Kunyit Laut Kecamatan Sungai Kunyit Kabupaten Mempawah,"},
            {"mempawah_pengkang","Pengkang merupakan olahan beras ketan yang diisi ebi lalu dibungkus dengan daun pisang membentuk kerucut kemudian dibakar. Pengkang ini sedap jika disantap bersama sambal kepah atau sambal kerang. Kepah adalah sejenis kerang yang hidup di hutan mangrove Kalimantan Barat."},
            {"mempawah_patlau","Patlau adalah makanan yang terbuat dari bahan beras ketan dengan proses perebusan yang cukup lama. Uniknya, makanan yang dibungkus dari daun pisang ini memiliki bentuk segitiga panjang. Bentuk patlau tesebut sendiri diperoleh dari hasil cetakan yang terbuat dari bahan kayu."},
        },
            //Singkawang
        {
            {"taman_pasir_panjang","Taman Pasir Panjang Indah (TPPI), Terletak di Kecamatan Singkawang Selatan Kelurahan Sedau yang berdiri sejak tahun 1973. Dari segi fisik, taman ini memiliki fasilitas seperti Palapa beach hotel, motel, diskotik, kolam renang, camping ground, sirkuit racing motor, kincir air, speed boat, banana boat. Selain itu taman ini terkenal juga dengan pasir putihnya yang bersih berserta pemandangannya yang menakjubkan."},
            {"bukit_bougenville","Bukit Bougenvil, Kawasan wisata ini berada 6 km sebelah Barat dari pusat Kota Singkawang. Terletak di wilayah perbukitan “Pangmilang” serta dikelilingi oleh gunung-gunung yang menjadikan bukit ini terasa sejuk dan nyaman. Dinamakan bukit Bougenville karena di area taman ini ditumbuhi dengan berbagai jenis spesies bunga bougenville yang bibitnya tidak saja berasal dari alam lokal, namun juga didatangkan dari luar negeri. Di taman bukit bougenville ini juga telah berhasil mengembangkan sebanyak 46 species tanaman bougenville. Kesejukan hawa alam pegunungan dan hamparan bunga bougenville yang tertata rapi menambah semaraknya suasana hati untuk berlibur bersama keluarga."},
            {"rindu_alam","Rindu Alam hanya berjarak 18 km dari wisata pasir Panjang dan palm beach. Lokasinya terletak di antara gunung bajau, gunung kota dan gunung pelapis. Pengunjung juga harus melalui jalanan Panjang yang berkelok-kelok sebelu mencapai puncak taman ini. Berada di ketinggian 400 m diatas permukaan laut, sensasi hawa dingin akan langsung terasa bagi mereka yang telah berhasil tiba di puncak taman rindu alam ini. Dari puncak inilah, kita dapat melihat keindahan panorama parawisata alam kota singkawang, mulai dari pemandangan kota yang dihiasi warna-warni seribu kelenteng, pemandangan hamparan pasir, beberapa pantai indah hingga pemandangan rimbun dan hijaunya tiga puncak gunung yang mengawal taman rindu alam."},
            {"singkawang_rujak_ebi","Rujak Ebi Singkawang terbuat dari aneka macam buah-buahan tropis yang disiram dengan saus kacang yang dihaluskan bersama dengan cabai rawit. Yang membedakan dari rujak kebanyakan adalah taburan ebi parut di atasnya sehingga citarasanya berpadu antara asam, segar, manis, pedas, dan asin."},
            {"singkawang_bubur_gunting","Bubur Gunting adalah salah satu makanan yang khas dari daerah Singkawang. Makanan ini terbuat dari roti goreng atau cakwe yang kemudian dipotong dengan cara digunting-gunting mungkin itulah salah satu sebabnya makanan ini dinamakan bubur gunting. Bubur ini biasanya memiliki kuah kental yang memiliki citarasa yang manis karena terbuat dari kacang hijau dan tepung kanji."},
            {"singkawang_mie_tiaw","Mie Tiaw Asu merupakan olahan mie panjang seperti pada umumnya, namun yang membedakan adalah ukuranya yang agak besar dan pipih. Asu dalam bahasa Mandarin yang berarti Paman. Mie Tiaw ini dimasak dengan cara ditumis dicampur dengan sayuran, toge, dan daging. Daging yang digunakan bermcam-macam, ada yang menggunakan daging sapi, ayam, seafood, boleh juga daging onta atau kuda"},
        },
            //Sambas
        {
            {"istana_alwatzikhoebillah","Istana Alwatzikhoebillah pertama kali didirikan oleh Raden Bima, gelar untuk Sultan Muhammad Tajuddin, Sultan Sambas ke-2 pada tahun 1632 M. Istana yang ada sekarang dibangun oleh Sultan Muhammad Mulia Ibrahim pada tahun 1933 yang ditempati pada tanggal 6 juli 1935. Memasuki muka istana terdapat bangunan masjid Jami’ Sambas yang telah berusia lebih dari 100 tahun. Di dalam istana terdapat barang pusaka peninggalan sultan-sultan sambas, diantaranya berbagai jenis senjata dan benda-benda religi lainnya."},
            {"sambas_tanjung_batu","Pantai yang merupakan bentukan alam berupa bukit berbatu yang menjorok ke laut (tanjung) ini, terletak hanya sekitar 200 m dari Kota pemangkat atau sekitar 47 km dari ibu kota kabupaten dan sekitar 178 dari ibu kota Provinsi Kalimantan Barat. Dari atas bukit, pengunjung dapat menikmati panorama seperti pemandangan kota Pemangkat, gunung gajah serta laut cina selatan."},
            {"temajuk","Temajuk merupakan desa kecil yang terletak persis di ekor pulau Kalimantan, yang memiliki destinasi wisata kelas dunia yang belum banyak diketahui. Primadona andalan Kabupaten Sambas ini bahkan tak pernah putus kunjungan wisatawan, baik lokal maupun dari negara tetangga, Malaysia. Temajuk terletak di Kecamatan Paloh, Kabupaten Sambas. Temajuk merupakan desa terakhir yang ada di pesisir utara Kalimantan Barat yang bersebelahan dan berbatasan langsung dengan Teluk Melano, Sarawak, Malaysia."},
            {"sambas_bubur_pedas","Suku Melayu Sambas di Kalimantan Barat memiliki unggulan kuliner pedas yang harus dicicipi para wisatawan, yakni Bubur Pedas Sambas. Dahulu bubur pedas ini disajikan di kerajaan, dan merupakan cerminan budaya yang kental di kerajaan Melayu Deli. Bubur pedas ini terbuat dari beras yang ditumbuk halus dioseng dan kaya akan rempah serta sayuran dan gorengan kacang tanah plus ikan teri yang digoreng kering menambah cita rasa."}
        },
            //Bengkayang
        {
            {"pulau_randayan","Pulau Randayan, Pulau ini memiliki daya tarik berupa kepulauan berpasir putih, pantai yang landai serta panorama kepulauan yang indah dengan kondisi air laut yang jernih dan bersih. Untuk menuju ke lokasi Pulau Randayan dapat dicapai dari Objek Wisata Taman Pasir Panjang Indah Singkawang dengan menggunakan speed boat kapasitas mencapai 6-20 orang waktu tempuh sekitar 45 menit. Berbagai kegiatan dapat dilakukan seperti outbound, meeting, memancing, diving, snorkeling dan jika beruntung kita dapat menemukan penyu yang sedang bertelur."},
            {"pulau_kabung","Pulau kabung merupakan salah satu pulau yang ada di pesisir Kabupaten Bengkayang, pulau lain yang berdekatan diantaranya adalah Pulau Lemukutan, Pulau Randayan, Pulau Penata Besar dan Pulau Penata Kecil. Gugusan pulau ini berhadapan langsung dengan laut natuna bagian barat. Luas kawasan pulau kabung  19,40 Ha dan terbagi menjadi empat bagian.Sebagai daerah tujuan wisata yang memiliki ciri khas tersendiri, ekowisata pulau ini memiliki taman laut yang menarik, perkebunan cengkeh masyarakat, teknik tradisional nelayan tangkap ikan, upacara adat masyarakat setempat serta kehidupan sosial dan ekonomi "},
            {"riam_merasap","Air terjun Riam Merasap adalah objek wisata alam berupa air terjun yang terletak di Kabupaten Bengkayang tepatnya di Desa Sahan, Kecamatan Seluas. Jarak dari kota Pontianak menuju air terjun ini sekitar 300 km dan dalam waktu sekitar kurang lebih 8 jam. Air terjun ini memiliki ketinggian sekitar 20 meter dan lebar sekitar 8 meter dengan air jernih yang mengalir di sana dan panorama rimba tropis khas Kalimantan yang membuat air terjun ini semakin indah."},
            {"bengkayang_lempok","Lempok adalah panganan khas Indonesia yang berasal dari durian. Lempok yaitu sejenis dodol tetapi tidak menggunakan bahan campuran seperti tepung atau 100% memakai bahan dasar buah durian saja. Jika memakai campuran tepung tentu namanya adalah dodol durian, sedangkan lempok hanya berbahan dasar durian saja."}
        },
            //Landak
        {
            {"rumah_betang","Rumah Betang atau biasa juga disebut rumah Panjang merupakan identitas etnik Dayak. Walaupun merupakan identitas, namun rumah Panjang di Kabupaten Landak saat ini hanya bisa ditemukan di Kampung Saham Kecamatan Sengah Temila. Menurut cerita orang tua terdahulu rumah Panjang Saham didirikan pada tahun 1875. Mulanya rumah Panjang yang dihuni oleh suku Dayak Kanayatn ini terdiri dari satu pintu. Seiring pertambahan keluarga, dibangunlah di bagian kiri dan kanan yang akhirnya terdiri dari 35 pintu."},
            {"keraton_ismahayana","Keraton Ismahayana (Ismahayana Sultan Palace) Kompleks Istana Kerajaan Landak yang terletak di kota Ngabang, Kabupaten Landak memiliki tiga elemen utama, yakni Istana Ismahayana Landak, Masjid Djami’ Keraton Landak, serta makam para Sultan Kerajaan Landak dan kerabatnya. Dirasa tak lengkap, bila wisatawan tidak mampir berziarah ke makam raja-raja Landak ketika berkunjung ke kompleks Keraton Landak Makam  para sultan dan para kerabatnya terletak di sebelah barat Masjid Djami’ Keraton Landak."},
            {"riam_banangar","Riam Banangar Terletak di Dusun Perbuah, Desa Marayuh Kecamatan Air Besar. Air terjun ini telah menjadi objek wisata yang cukup banyak dikunjungi wisatawan lokal. Perjalanan melintasi sungai landak ini memakan waktu sekitar dua setengah jam, lamanya waktu ditempuh harus melewati sejumlah riam. Air terjun Banangar merupakan patahan sungai landak, tingginya sekitar 40 meter dan lebarnya sekitar 60 meter. Di atasnya terdapat dahan pepohonan, yang menurut warga setempat, sudah bertahun tahun ada di sana  dan tidak jatuh atau hanyut. Air terjun Manggar dikelilingi gunung pejapa (1.109 m). Selain adanya air terjun, di seberang sungai juga terdapat gua sanjan. Hutan alami di sekitar air terjun menyimpan bermacam varietas anggrek hutan dan kantong semar langka dan unik."},
            {"landak_rotikap","Rotikap merupakan kue asli melayu Kabupaten Landak yang dibuat dengan bahan utamanya buah kelapa yang diambil santannya. Mirip seperti biskuit, namun dengan tekstur yang lebih lembut. Rotikap juga tidak mengandung mentega seperti kue pada umumnya. Disebut juga sebagai kue yang paling populer, karena rasa khasnya yang manis dan harum, serta paling digemari terutama saat menjelang hari-hari besar, baik itu Idul Fitri, Idul Adha, maupun hari Natal."},
        },
            //Sanggau
        {
            {"pancur_aji","Daya tarik wisata Pancur Aji merupakan Kawasan wisata yang dikelola oleh pemerintahan daerah kabupaten sanggau dengan jarak lokasi dari ibukota kabupaten hanya sekitar 3 km dan sekitar 300 km dari ibukota provinsi kalbar. Daya tarik utama di Kawasan ini adalah berupa air terjun yaitu air terjun Engkuli dan air terjun Setapang, karena daerah ini merupakan wilayah konservasi dengan luas sekitar 33 Ha, sehingga dilokasi ini banyak terdapat tanaman langka yang dilindungi seperti hutan tengkawang, pekawai, albasia, anggrek hutan dan masih banyak lagi tanaman lainnya."},
            {"ai_sipa","Masyarakat setempat menyebut air terjun ini Ai’ Sipatn Lotup yang artinya Air Mendidih, dikarenakan air panas ini mengandung sulphur atau belerang sedangkan di Kabupaten Sanggau dan sekitarnya tidak terdapat gunung berapi aktif. Sumber air panas ini berlokasi di Desa Sape, Dusun Peruntan Kecamatan Jangkang Kabupaten Sanggau atau sekitar 50 km dari ibukota kabupaten, dengan luas areal wilayah sekitar 2 Ha."},
            {"keraton_suryanegara","Keraton Suryanegara, Kerajaan Sanggau merupakan salah satu kerajaan tertua di Indonesia, didirikan oleh Putri Daranante dan Babai Cinga pada abad ke 13 (Sekitar 700 tahun yang lalu). Salah satunya peninggalan Kerajaan Sanggau yang masih berdiri kokoh sampai saat ini adalah bangunan Keraton yang bernama Keraton Suryanegara. Hal ini oleh pemerintahan dijadikan benda cagar budaya"},
            {"sanggau_sungkui","Sungkui adalah salah satu menu wajib yang dikonsumsi oleh masyarakat di Kabupaten Sanggau pada perayaan hari raya. Sungkui merupakan makanan khas Sanggau berbentuk mirip lontong yang dibungkus dengan daun keririt atau yang juga sering disebut daun sungkui dan daun sanggau hingga memiliki rasa dan aroma yang khas."},
            {"sanggau_kote","Kote merupakan kue tradisional Sanggau sejenis pastel yang di dalamnya berisi serundeng kelapa."},
        },
            //Sekadau
        {
            {"batu_bertulis","Batu Bertulis berukuran tinggi 3,90 meter dan lebar 5,10 meter yang ada di Kabupaten Sekadau merupakan bukit tertulis sejarah Indonesia kuno, batu ini terletak di Desa Sebabas, Kecamatan Nanga Mahap lebih kurang 18 km dari ibu kota kecamatan, Pada batu alam ini terdapat tulisan secara vertikal dengan menggunakan huruf Pallawa berbahasa Sanksekerta tahun 650 Masehi atau sekitar abad 7 M. Tulisan yang ada di batu  tersebut berisikan doa dan mantera untuk keselamatan dan kesejahteraan masyarakat di sekitar lokasi batu tertulis."},
            {"lawang_kuari","Goa Lawang Kuari Goa ini terletak tepat di tebing Sungai Kapuas, mempunyai 3 buah lobang, dengan mulut goa  yang sangat kecil sehingga hanya orang-orang tertentu saja yang dapat masuk ke dalam goa. Bagi yang berhasil masuk ke dalam goa, maka akan menemukan lapangan yang luas dan tinggi serta danau. Lokasi goa ini terdapat di Desa Seberang Kapuas Kecamatan Sekadau Hilir atau lebih kurang 5 km dari ibu kota kecamatan dengan menggunakan sarana angkutan air."},
            {"gurong_sumpit","Air Terjun Gurong Sumpit Air terjun ini terletak di Desa Merbang Kecamatan Belitang Hilir atau sekitar 47 km dari ibu kota kabupaten, dengan ketinggian sekitar 10 meter dan lebar 5 meter. Dari ibu kota kabupaten ke ibu kota kecamatan dapat menggunakan kendaraan roda 4, sedangkan dari ibu kota kecamatan menuju lokasi harus melalui jalan setapak dengan berjalan kaki sekitar 40 menit. Pada dinding batu tempat air terjun, terdapat sebuah goa dengan kedalaman sekitar 50 meter,"},
            {"sekadau_lemang","Seperti halnya dengan suku lain yang ada di Indonesia, suku dayak Kalimantan Barat juga memiliki kuliner khas yang lezat dan nikmat. Kuliner yang terkenal kelezatannya dari suku dayak ini yaitu nasi lemang. Makanan yang lezat ini berbahan dasar dari ketan yang diolah dengan cara yang unik. Pertama dengan mengolah beras ketan bersama dengan bumbu-bumbu khas suku dayak,"},
            {"lulun_koe_sekadau","Koe lulun merupakan makanan sejenis lepat yang isinya gula merah, terdapat di daerah Belitang Kabupaten Sekadau."},
        },
           
            //Melawi
        {
            {"bukit_baka","Taman Nasional Bukit Baka – Bukit Raya Merupakan Kawasan Taman Nasional terbesar kedua yang ada di Kalimantan Barat setelah Betung Kerihun. Taman ini terletak di dua Provinsi, Provinsi Kalbar dan Kalteng. Taman Nasional Bukit Baka/Bukit raya ini mempunyai dua gerbang untuk menuju ke area zona inti, yang pertama melalui pintu gerbang Nanga Nuak dengan memanfaatkan areal tracking menuju Bukit Asing, sedangkan gerbang kedua dari arah Nanga Juoi."},
            {"danau_lintah","Danau Lintah, menurut cerita penduduk setempat, danau lintah dulunya merupakan sebuah sungai. Sungai tersebut menyempit dikarenakan adanya pembangunan jalan oleh sebuah perusahaan, lama kelamaan area tersebut menjadi danau karena airnya tidak mengalir atau tergenang serta ditumbuhi tanaman liar dan tertutup. Yang menarik adalah danau tersebut diapit oleh bukit-bukit kecil yang gundul, juga masih digunakan sebagai tempat mandi, mencuci bahkan mencari ikan. Letak Danau Lintah kurang lebih 8 km dari Kota Nanga Pinoh."},
            {"guong_nobonk","Guongk Nobongk Air terjun Nobongk adalah salah satu destinasi wisata alam yang berada di desa Poring Kecamatan Nanga Pinoh, Kabupaten Melawi, Kalimantan Barat. Kalian bisa menikmati suasana sejuknya udara yang diproduksi oleh pohon-pohon tinggi menjulang serta air yang jernih dan sangat cocok untuk spot selfie dan tempat panjang tebing bagi para pencinta alam yang suka berpetualangan di alam bebas. Karena keberadaanya yang sangat tersembunyi berada di tengah hutan membuat guongk nobongk belum dikenal luas dan jarang dikunjungi"},
            {"sintang_tempoyak","Oncau ikan, Setiap daerah tentu saja memiliki makanan khas, termasuk kabupaten Melawi Kalimantan Barat, yang memiliki beberapa makanan khas oncau ikan. Bahan utamanya adalah buah durian yang dicampur dengan ikan, sehingga tidak sedikit pula masyarakat yang menyebutnya dengan Tempoyak Ikan."},
            {"melawi_sibung","Sibung merupakan sejenis tumbuhan liar seperti lengkuas, namun batang pohonnya lebih tinggi. Sibung yang dapat diolah menjadi makanan adalah sibung yang masih muda atau kuncup, seperti halnya rebung. Sibung bisa dijadikan sayur ataupun dijadikan sambal. Dengan rasa dan aroma yang khas, sibung menjadi makanan unik yang perlu dicoba."},
        },

         //Sintang
         {
            {"bukit_kelam","Bukit Kelam merupakan kawasan wisata bukit yang berada di wilayah Kecamatan Kelam Permai, Kabupaten Sintang. Bukit ini memiliki keindahan yang khas, terletak pada ketinggian 50-900 meter dari permukaan laut dengan kemiringan antara 15-40 derajat serta kemiringan diatas 45 derajat. Taman wisata bukit kelam ini terletak antara 2 bantaran sungai besar di Kalimantan Barat yaitu sungai Kapuas dan sungai Melawi."},
            {"nokan_nayan","Air Terjun Nokan Nayan Objek wisata yang cukup menakjubkan lainnya terletak di sektor selatan. Air terjun yang berketinggian 250 meter dari dasar sungai yaitu air terjun Nokan Nayan. Nokan artinya air terjun dan Nayan berarti nama sebuah sungai yang merupakan sumber air. Sumber air tersebut mengaliri Nokan Nayan yang terletak di Kecamatan Ambalau (40 km dari desa Ambalau). Air terjun ini terletak pada pertemuan dua aliran Sungai Jenggonoi dan Sungai Nayan. Di lokasi air terjun ini terdapat bangunan tradisional Tojahan yang berarti tempat pemujaan bagi Hatalak (sang pencipta)."},
            {"baning","Hutan Wisata Baning Hutan ini sangat alami yang terletak di tengah-tengah Kota Sintang, luasnya kurang lebih 215 Ha. Hutan Wisata Baning merupakan hutan tropis, termasuk hutan yang utuh kaya akan aneka ragam flora dan fauna, seperti kantong semar, anggrek hutan, kelasi dan berbagai jenis burung dan orang utan. "},
            {"sintang_pekasam","Pekasam adalah makanan khas Sintang yang terbuat dari ikan yang ditaburi dengan garam, disimpan di suatu wadah yang memiliki tutup, dan dibiarkan beberapa hari sehingga menjadi pekasam."},
            {"sintang_semprong","Kue semprong merupakan salah satu makanan khas kabupaten Sintang. Semprong dapat dicetak dengan berbagai bentuk, namun umumnya semprong berbentuk silinder kecil memanjang. Selain itu, ada juga semprong yang dilipat menjadi segitiga ataupun persegi panjang."},
        },
            //Kapuas Hulu
        {
            {"danau_sentarum","Danau Sentarum umumnya berbentuk cekungan datar atau lebak lebung yang merupakan daerah hamparan banjir yang dikelilingi oleh jajaran pegunungan. Dari kawasan Taman Nasional Danau Sentarum tercatat hingga saat ini terdapat 675 spesies flora yang tergolong dalam 97 familia. Yang paling mengagumkan di danau sentarum terdapat jenis tumbuhan yang sama dengan tumbuhan  endemik yang ada di hutan Amazon."},
            {"betung_kerihun","Taman Nasional Betung Kerihun merupakan kawasan konservasi terbesar di Provinsi Kalimantan Barat dengan luas 800.000 hektar atau sekitar 5,5% dari luas total daratan Provinsi Kalimantan Barat. Taman ini memiliki 4 subdestinasi yang dapat dikunjungi yaitu destinasi Embaloh, destinasi Mendalam, destinasi Sibau dan destinasi Kapuas. Destinasi Embaloh merupakan daerah sebaran habitat dari satwa-satwa kunci kawasan taman ini yaitu orang utan, burung enggang, merak kalimantan dan ikan semah."},
            {"danau_empangau","Danau Empangau terletak di Desa Nanga Empangau, Kecamatan Bunut Hilir, Kabuaten Kapuas Hulu. Danau Empangau ditetapkan sebagai danau lindung melalui SK Bupati No. 6 tahun 2001. Luasnya mencapai 124 hektar dengan kedalaman 3-21 meter. Di kawasan lindung, kedalamannya bisa mencapai 17,5 meter. Dengan suhu air berkisar antara 28-32 derajat celcius menjadikan danau ini sebagai habitat yang cocok untuk berbagai jenis ikan seperti arwana, siluk, toman, jelawat, ringau, tengadak dan baung."},
            {"kapuas_hulu_kerupuk_basah","Kerupuk basah atau biasa juga disebut temet merupakan makanan tradisional khas Kapuas Hulu, teksturnya kenyal dan rasanya gurih yang terbuat dari ikan air tawar. Temet biasanya disajikan sebagai sambal di kalangan masyarakat Kapuas Hulu yang bermukim di kawasan Danau Sentarum."},
        },
            //Ketapang
        {
            {"keraton_saunan","Keraton G.M Saunan dan makam Keramat Tujuh, Tepat di Desa Mulia Kerta Kecamatan Benua Kayong Kabupaten Ketapang, kuramg lebih 4 km dari pusat ibu kota kabupaten, terdapat sebuah keraton, yaitu Keraton Panembahan Kerajaan Matan G.M Saunan yang hingga saat ini masih terjaga kelestariannya. Daya Tarik yang ada di Keraton ini adalah kita dapat menjumpai peninggalan Panembahan Raja G.M Saunan yang sudah berumur ratusan tahun, diantaranya yang paling terkenal adalah Meriam Padam Pelita."},
            {"tanjung_belandang","Pantai Tanjung Belandang merupakan salah satu objek wisata pantai yang ada di Ketapang. Pesona Pantai Tanjung Belandang tidak kalah menariknya dengan pesona pantai yang ada di daerah lain. Objek wisata ini merupakan objek wisata pantai yang tidak jauh dengan pusat kota Ketapang. Lokasi objek wisata ini hanya berjarak 12 km dari pusat Kota Ketapang tepatnya berada di Desa Sungai Awan Kanan Kecamatan Muara Pawan Kabupaten Ketapang"},
            {"pantai_air_mati","Pantai Air Mati atau sering disebut juga dengan pantai Air Mata Permai berlokasi tidak jauh dari pusat kota yaitu hanya berjarak 10 Km dan dapat ditempuh dengan menggunakan kendaraan roda 2 dan roda 4 dalam waktu tempuh hanya 10 menit dari pusat kota Ketapang. Sedangkan luas pantai ini sekitar 100 Ha dan diantaranya terdapat hutan lindung. Pantai ini didominasi oleh tanaman Mangrove. Di dalam hutan mangrove inilah  tempat persinggahan burung-burung dari berbagai daerah dan negara."},
            {"ketapang_ale","Ale-ale adalah sejenis hewan air yang menyerupai kerang. Hewan air ini juga dijadikan simbol atau tagline kota Ketapang. Ale-ale dapat diolah menjadi berbagai macam masakan seperti sambal ale-ale, ale-ale kuah kental, ale-ale asam garam, sate ale-ale dan lain-lain. Cara sederhana untuk menikmati ale-ale adalah direbus dengan garam. Selain memiliki rasa yang lezat, ale-ale juga memiliki kadar protein yang tinggi sehingga baik untuk kesehatan."},
            {"ketapang_amplang","Amplang adalah kerupuk khas Ketapang yang saat ini sangat disukai dan dapat menjadi buah tangan bagi wisatawan yang berasal dari luar daerah. Amplang paling cocok dimakan bersama dengan nasi beserta sayur dan lauk. Amplang terbuat dari bahan dasar ikan tenggiri atau ikan belida yang digiling halus dengan campuran tepung sagu."},
        },
        //Kayong Utara
        {
            {"riam_merasap","Air terjun Riam merasap, terletak di kaki Gunung Seberuang hulu Sungai Siduk Dusun Pangkal Tapang Desa Riam Berasap Jaya Kecamatan Sukadana. Air terjun ini memiliki ketinggian kurang lebih 20 meter dengan lebar 10 m dan terletak pada sungai yang menyerupai kolam berdiameter 35 m dengan kedalam 5-8 m. Ketinggian air terjun dan kedalaman kolam ini dipengaruhi oleh pasang surut. Fenomena ketika air terjun jatuh ke Sungai Siduk menyebabkan percikan uap air berupa kabut putih tebal yang menyerupai asap, sehingga air terjun ini diberi nama Air Terjun Riam Berasap."},
            {"pantai_pulau_datuk","Pantai Pulau Datuk Pantai Pulau Datuk merupakan salah satu primadona wisatawan, lokasi objek wisata pantai pulau datuk ini berdekatan dengan Taman Nasional Gunung Palung, Pasir Mayang serta Pantai Tambak Rawang. Pantai Pulau Datuk terletak di kaki Gunung Peramas. Pantai ini berpasir halus berbentuk teluk dengan kecekungan cukup dalam. Selain itu pantai pulau datuk juga memiliki kelandaian yang cukup baik dengan kondisi permukaan yang cukup bervariasi."},
            {"gunung_palung","Taman Nasional Gunung Palung (TNGP) memiliki luas kurang lebih 90.000 Ha. Potensi sumber daya alam yang menjadikan kawasan ini memiliki daya tarik khusus adalah keberadaan tipe ekosistemnya yang lengkap. Mulai dari tipe vegetasi hutan, pantai, hutan mangrove, hutan rawa gambut, hutan dataran rendah hingga vegetasi hutan puncak pegunungan. Jenis flora yang dapat dijumpai di kawasan TNGP sangat beragam. Mulai dari tanaman keras berkayu, belukar hingga beragam tanaman hias."},
            {"kayong_utara_ketupat","Ketupat colet adalah menu wajib saat lebaran, baik ketika Idul Fitri maupun Idul Adha, namun tak jarang ketupat colet disajikan juga saat acara syukuran dan acara besar lainnya. Bagi masyarakat melayu Kayong Utara, tak lengkap rasanya lebaran jika tidak memakan ketupat colet. Yang membuatnya semakin unik adalah makanan ini memiliki berbagai bentuk seperti ketupat bawang merah, ketupat biasa, ketupat bawang putih, ketupat jontong, ketupat kumbek dll. Sebenarnya ketupat colet ini sama dengan ketupat pada umumnya, yang membedakan hanya cara penyajiannya saja."},
        },
    };
    //music
    private String[] music = new String[]{
        "pontianak",
        "kubu_raya",
        "mempawah",
        "singkawang",
        "sambas",
        "bengkayang",
        "landak",
        "sanggau",
        "sekadau",
        "sintang",
        "melawi",
        "kapuas",
        "ketapang",
        "kayong",
    };

    private String[][] puzzle = new String[][]{
        //Pontianak
            {"pontianak_insang.png","pontianak_chaikwe.jpg"},
        //Kubu Raya
            {"kubu_raya_batik_layar_meretas.jpg","kubu_raya_sale.jpg"},
        //Mempawah
            {"mempawah_awan_berarak.jpg","mempawah_pengkang.jpg"},
        //Singkawang
            {"singkawang_tenun_ikat.jpg","singkawang_sape.jpg"},
        //Sambas
            {"sambas_bubur_pedas.jpg","sambas_bunge_tanjung_JPG","sambas_selendang_pucuk_rebung.JPG"},
        //bengkayang
            {"bengkayang_batik_saruwe_kamale.jpg","bengkayang_lempok.jpg"},
        //landak
            {"landak_betang.jpg"},
        //sanggau
            {"sanggau_sulaman_kalengkang.jpg","sanggau_sungkui.jpg"},
        //sekadau
            {"sekadau_gurung_sumpit.jpg"},
        //melawi
            {"melawi_mandau.jpg","melawi_bukit_baka.jpg"},
        //sintang
            {"sintang_tidayu.jpg","sintang_tempoyak_ikan.jpg"},
        //Kapuas Hulu
            {"kapuas_tenun_ibas.jpg","kapuas_hulu_kerupuk_basah.jpg"},
        //ketapang
            {"ketapang_keraton_matan.jpg"},
        //kayong utara
            {"kayong_utara_batik.jpg","kayong_utara_ketupat.jpg"},
    };

    public String[][] getQuestion() {
        return Question;
    }

    public String[][][] getAnswer() {
        return Answer;
    }

    public String[][] getmAnswer() {
        return mAnswer;
    }

    public String[][][] getTebak_gambar() {
        return tebak_gambar;
    }

    public String[][] getInfo_primary() {
        return info_primary;
    }

    public String[][][] getInfo_secondary() {
        return info_secondary;
    }

    public String[] getMusic() {
        return music;
    }

    public String[][] getPuzzle() {
        return puzzle;
    }

    public static String[] getLabel_kota() {
        return Label_kota;
    }
}
