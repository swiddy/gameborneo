package emcorp.studio.gameborneo.Library;

public class Route {
    private int nextroute;
    private int currentroute;
    private int currentcity;
    public static int finishRoute = 99;

    public Route(int currentroute, int currentcity) {
        this.currentroute = currentroute;
        this.currentcity = currentcity;
    }



    public int getNextroute() {
        switch (currentroute){
            case 0:
                switch (currentcity){
                    case Constant.PONTIANAK:
                        nextroute = Constant.KUBU_RAYA;
                        break;
                    case Constant.KUBU_RAYA:
                        nextroute = Constant.MEMPAWAH;
                        break;
                    case Constant.MEMPAWAH:
                        nextroute = Constant.SINGKAWANG;
                        break;
                    case Constant.SINGKAWANG:
                        nextroute = Constant.SAMBAS;
                        break;
                    case Constant.SAMBAS:
                        nextroute = finishRoute;
                        break;
                }
                break;


            case 1:
                switch (currentcity){
                    case Constant.PONTIANAK:
                        nextroute = Constant.MEMPAWAH;
                        break;
                    case Constant.MEMPAWAH:
                        nextroute = Constant.LANDAK;
                        break;
                    case Constant.LANDAK:
                        nextroute = Constant.BENGKAYANG;
                        break;
                    case Constant.BENGKAYANG:
                        nextroute = finishRoute;
                        break;
                }
                break;


            case 2:
                switch (currentcity){
                    case Constant.PONTIANAK:
                        nextroute = Constant.KUBU_RAYA;
                        break;
                    case Constant.KUBU_RAYA:
                        nextroute = Constant.KAYONG_UTARA;
                        break;
                    case Constant.KAYONG_UTARA:
                        nextroute = Constant.KETAPANG;
                        break;
                    case Constant.KETAPANG:
                        nextroute = finishRoute;
                        break;
                }
                break;


            case 3:
                switch (currentcity){
                    case Constant.PONTIANAK:
                        nextroute = Constant.KUBU_RAYA;
                        break;
                    case Constant.KUBU_RAYA:
                        nextroute = Constant.SANGGAU;
                        break;
                    case Constant.SANGGAU:
                        nextroute = Constant.SEKADAU;
                        break;
                    case Constant.SEKADAU:
                        nextroute = Constant.MELAWI;
                        break;
                    case Constant.MELAWI:
                        nextroute = Constant.SINTANG;
                        break;
                    case Constant.SINTANG:
                        nextroute = Constant.KAPUAS_HULU;
                        break;
                    case Constant.KAPUAS_HULU:
                        nextroute = finishRoute;
                        break;
                }
                break;
        }
        return nextroute;
    }
}
