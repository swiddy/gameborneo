package emcorp.studio.gameborneo.Library;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class PlayAudio extends Service{
    private static final String LOGCAT = "cetak";
    MediaPlayer objPlayer;

    public void onCreate(){
        super.onCreate();
        Log.d(LOGCAT, "Service Started!");
    }

    public int onStartCommand(Intent intent, int flags, int startId){
        String song = intent.getExtras().getString("song");
        int songId = getResources().getIdentifier(song, "raw", getApplicationInfo().packageName);
        objPlayer = MediaPlayer.create(this, songId);
        objPlayer.setVolume(1, 1);
        objPlayer.start();
        Log.d(LOGCAT, "Media Player started!");
        if(objPlayer.isLooping() != true){
            Log.d(LOGCAT, "Problem in Playing Audio");
        }
        return 1;
    }

    public void onStop(){
        objPlayer.stop();
        objPlayer.release();
    }

    public void onPause(){
        objPlayer.stop();
        objPlayer.release();
    }
    public void onDestroy(){
        if (objPlayer != null) objPlayer.release();
    }

    @Override
    public IBinder onBind(Intent objIndent) {
        return null;
    }
}