package emcorp.studio.gameborneo.Library;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;

public class MusicReceiver extends BroadcastReceiver {
    MediaPlayer objPlayer;
    private Context context;
    private Intent intent;
   // private int songId;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        this.context = context;
        this.intent = intent;
       // this.songId = intent.getIntExtra("song",0);

        String action = intent.getAction().toString();
        Log.d("Data",action+" "+intent.getStringExtra("status"));
        if (action.equals("emcorp.studio.gameborneo.musicreceiver")){
            switch (intent.getStringExtra("status")){
                case "play":
                    playMusic(context,intent.getIntExtra("song",0));
                    break;
                case "pause":
                    PauseMusic();
                    break;
                case "stop":
                    StopMusic(context,intent.getIntExtra("song",0));
                    break;
                case "resume":
                    ResumeMusic(context,intent);
                    break;
                case "next":
                    playNext(context,intent.getIntExtra("song",0));
                    break;

            }
        }
//        throw new UnsupportedOperationException("Not yet implemented");
    }



    public void playMusic(Context context, int songId){
        objPlayer = MediaPlayer.create(context, songId);
        objPlayer.setVolume(1, 1);
        objPlayer.start();

    }

    public void playNext(Context context, int songId){
    //   objPlayer.stop();
     //  objPlayer.reset();
       objPlayer = MediaPlayer.create(context, songId);
       objPlayer.setVolume(1, 1);
       objPlayer.start();

    }

    public void ResumeMusic(Context context,Intent intent){
        if(objPlayer!=null) {
            if (!objPlayer.isPlaying()) {
                objPlayer.start();
            }
        }else if(objPlayer==null){
            playMusic(context,intent.getIntExtra("song",0));
        }

    }

    public void StopMusic(Context context,int songId){

        //if (objPlayer.isPlaying()) {
            objPlayer.stop();
            objPlayer.release();
      //  }

    }

    public void PauseMusic(){
        if(objPlayer!=null) {
            if (objPlayer.isPlaying()) {
                objPlayer.pause();
            }
        }
    }
}
