package emcorp.studio.gameborneo.Library;

import android.content.Context;
import android.content.SharedPreferences;

public class SpScore {
    private Context ctx;
    private SharedPreferences spf;
    private SharedPreferences.Editor editor;
    private int score;

    public SpScore(Context ctx) {
        this.ctx = ctx;
        this.spf = ctx.getSharedPreferences("myspf",Context.MODE_PRIVATE);
        this.editor = spf.edit();
    }

    public boolean setScore(int score){
        editor.putInt("score",score);
        editor.apply();
        return  true;
    }

    public boolean deleteScore(){
        editor.remove("score").commit();
        return true;
    }

    public int getScore(){
        return spf.getInt("score",-1);
    }
}
