package emcorp.studio.gameborneo.game;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.poovam.pinedittextfield.SquarePinField;
import com.poovam.pinedittextfield.PinField;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Random;

import emcorp.studio.gameborneo.KotaActivity;
import emcorp.studio.gameborneo.Library.Constant;
import emcorp.studio.gameborneo.Library.PlayAudio;
import emcorp.studio.gameborneo.Library.Route;
import emcorp.studio.gameborneo.Library.SpScore;
import emcorp.studio.gameborneo.R;
import emcorp.studio.gameborneo.game.puzzle.PuzzleActivity;

public class TebakGambarActivity extends AppCompatActivity {

    String[][][] tebakgambar;
    String[][] question;
    private String[][] info_primary;
    TextView tvHints;
    LinearLayout llTebakGambar;
    ImageView imgBackground;

    private int rute;
    private int idkota;
    private String kota;

    private String[] music;

    int i;
    int randomquestion;
    String jawaban;
    String gambar;
    ImageView ivQuestion;

    int isStart;
    int songId;

    Handler handler = new Handler();

    private String ALERT_WRONG_ANSWER = "Jawaban anda salah coba lagi!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tebak_gambar);
        tvHints = (TextView) findViewById(R.id.tvHints);
        ivQuestion = (ImageView) findViewById(R.id.ivquestion);
        llTebakGambar = (LinearLayout)findViewById(R.id.llTebakGambar);
        imgBackground = (ImageView)findViewById(R.id.imgBackground3);
        try{
            this.idkota = Integer.parseInt(getIntent().getStringExtra("idkota"));
            this.kota = getIntent().getStringExtra("kota");
            this.rute = Integer.parseInt(getIntent().getStringExtra("rute"));

            Constant consta = new Constant();
            tebakgambar = consta.getTebak_gambar();
            this.info_primary = consta.getInfo_primary();
            question = tebakgambar[idkota];
            randomquestion = RandomNumberQuestion();
            jawaban = tebakgambar[idkota][randomquestion][1];
            gambar = tebakgambar[idkota][randomquestion][0];
            tvHints.setText(tebakgambar[idkota][randomquestion][2]);

            int backgroundImg = getResources().getIdentifier(info_primary[idkota][0], "drawable", getApplicationInfo().packageName);
            imgBackground.setImageResource(backgroundImg);

            this.music = consta.getMusic();

            int i_gambar = getResources().getIdentifier(gambar.toLowerCase(), "drawable", getApplicationInfo().packageName);
            ivQuestion.setImageResource(i_gambar);

            final SquarePinField squarePinField = findViewById(R.id.etJawaban);
            squarePinField.setNumberOfFields(jawaban.length());

            squarePinField.setFieldColor(Color.GRAY);
            squarePinField.setBackgroundColor(Color.WHITE);

            squarePinField.setOnTextCompleteListener(new PinField.OnTextCompleteListener() {
                @Override
                public boolean onTextComplete(@NotNull String enteredText) {
                    //Toast.makeText(getApplicationContext(),enteredText.toLowerCase()+"=="+jawaban.toLowerCase(),Toast.LENGTH_SHORT).show();
                    if (Objects.equals(enteredText.toLowerCase(), jawaban.toLowerCase())) {
                        // Toast.makeText(getApplicationContext(),"You right",Toast.LENGTH_SHORT).show();
                        
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 3 second
                                ShowDialog("Anda benar apakah lanjut?");
                            }
                        }, 1000);

                    } else {
                        Toast.makeText(getApplicationContext(), ALERT_WRONG_ANSWER, Toast.LENGTH_SHORT).show();
                        if(i==2){
                            
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // Do something after 3 second
                                    squarePinField.setText(ClueAnswer(jawaban));
                                }
                            }, 2000);
                        }
                        i += 1;
                    }
                    return true;
                }
            });

            isStart=0;
            ResumeAudio();
            songId = getResources().getIdentifier(music[idkota], "raw", getApplicationInfo().packageName);
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }

    }

    public void ShowDialog(String p_pesan_dialog) {
        UpDateScore();
        AlertDialog.Builder a_builder = new AlertDialog.Builder(TebakGambarActivity.this);
        a_builder.setMessage(p_pesan_dialog)
                .setCancelable(false)
                .setPositiveButton("Lanjutkan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(getApplicationContext(), PuzzleActivity.class);
                        // intent.putExtra("assetName","pontianak_chaikwe.jpg");
                        intent.putExtra("rute", String.valueOf(rute));
                        intent.putExtra("idkota", String.valueOf(idkota));
                        intent.putExtra("kota", kota);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        next();
                    }
                });
        AlertDialog alert = a_builder.create();
        alert.setTitle(null);
        alert.show();
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private int RandomNumberQuestion() {
        String[][] sizeQuestion = question;
        //  Toast.makeText(getApplicationContext(),"ukuran arry"+sizeQuestion.length,Toast.LENGTH_SHORT).show();
        return (int) (Math.random() * (sizeQuestion.length - 1));
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    public void UpDateScore() {
        SpScore spscore = new SpScore(this);
        int lastScore = spscore.getScore();
        int updateScore = lastScore + 30;
        //Toast.makeText(this,""+updateScore,Toast.LENGTH_SHORT).show();
        spscore.setScore(updateScore);
    }

    public void next() {
        Route route = new Route(rute, idkota);
        int nextroute = route.getNextroute();
        //Toast.makeText(getApplicationContext(),""+nextroute,Toast.LENGTH_SHORT).show();
        Intent audio = new Intent(getApplicationContext(), PlayAudio.class);
        stopService(audio);

        if (nextroute == 99) {
            Intent intent = new Intent(TebakGambarActivity.this, ScoreActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(TebakGambarActivity.this, KotaActivity.class);
            intent.putExtra("rute", String.valueOf(rute));
            intent.putExtra("idkota", String.valueOf(nextroute));
            intent.putExtra("kota", kota);
            startActivity(intent);
            finish();
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Data","onPause");
        if (isStart >= 0){
            PauseAudio();
        }

        isStart++;

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Data","onResume isStart="+isStart);
        if (isStart > 0){
            ResumeAudio();
        }
    }


    public void PauseAudio(){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","pause");
        sendBroadcast(in);
    }


    public void ResumeAudio(){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","resume");
        sendBroadcast(in);
    }


    public String ClueAnswer(String value){

        char[] huruf = new char[value.length()] ;
       final int[] ints = new Random().ints(value.length(),1, value.length()-1).distinct().limit(4).toArray();

        for(int i = 0;i<value.length();i++){
            if(i==ints[0]){
                huruf[i] = value.charAt(i);
            }
            else if(i==ints[1]){
                huruf[i] = value.charAt(i);
            }
            else if(i==ints[2]){
                huruf[i] = value.charAt(i);
            }else{
                huruf[i] = '*';
            }

        }

        return String.valueOf(huruf);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


}
