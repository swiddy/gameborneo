package emcorp.studio.gameborneo.game;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import emcorp.studio.gameborneo.KotaActivity;
import emcorp.studio.gameborneo.Library.Constant;
import emcorp.studio.gameborneo.Library.MusicReceiver;
import emcorp.studio.gameborneo.Library.PlayAudio;
import emcorp.studio.gameborneo.Library.Route;
import emcorp.studio.gameborneo.Library.SpScore;
import emcorp.studio.gameborneo.R;
import emcorp.studio.gameborneo.game.puzzle.PuzzleActivity;

public class PertanyaanActivity extends AppCompatActivity {
    private int rute;
    private int idkota;
    private String kota;
    Button btnAnswer1, btnAnswer2, btnAnswer3;
    TextView tvQuestion;
    ImageView imgBackground;
    String[][] manswer;
    String[][][] answer;
    String[][] question;
    private String[][] info_primary;
    int randomquestion;
    private int Kota;
    int isStart;
    int songId;


    private String[] music;
    private String ALERT_WRONG_ANSWER = "Jawaban Anda Salah Coba Lagi!";

    private Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pertanyaan);
        init();
        try{
            this.idkota = Integer.parseInt(getIntent().getStringExtra("idkota"));
            this.kota = getIntent().getStringExtra("kota");
            this.rute = Integer.parseInt(getIntent().getStringExtra("rute"));

            Constant consta = new Constant();
            question = consta.getQuestion();
            answer = consta.getAnswer();
            manswer = consta.getmAnswer();
            randomquestion = RandomNumberQuestion(Kota, question[idkota].length);
            info_primary = consta.getInfo_primary();
            //Log.d("Data", "index random=" + randomquestion + "Array length=" + question[idkota].length);
            this.music = consta.getMusic();

            tvQuestion.setText(question[idkota][randomquestion]);
            btnAnswer1.setText(answer[idkota][randomquestion][0]);
            btnAnswer2.setText(answer[idkota][randomquestion][1]);
            btnAnswer3.setText(answer[idkota][randomquestion][2]);

            int backgroundImg = getResources().getIdentifier(info_primary[idkota][0], "drawable", getApplicationInfo().packageName);
            imgBackground.setImageResource(backgroundImg);

            songId = getResources().getIdentifier(music[idkota], "raw", getApplicationInfo().packageName);

            btnAnswer1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (btnAnswer1.getText() == manswer[idkota][randomquestion]) {
                        Toast.makeText(getApplicationContext(), "right answer", Toast.LENGTH_SHORT).show();

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 3 second
                                ShowDialog("Anda benar apakah lanjut?");
                            }
                        }, 1000);
                    } else if (btnAnswer1.getText() != manswer[idkota][randomquestion]) {
                        Toast.makeText(getApplicationContext(), ALERT_WRONG_ANSWER, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            btnAnswer2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (btnAnswer2.getText() == manswer[idkota][randomquestion]) {
                        Toast.makeText(getApplicationContext(), "Right answer", Toast.LENGTH_SHORT).show();

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 3 second
                                ShowDialog("Anda benar apakah lanjut?");
                            }
                        }, 1000);
                    } else if (btnAnswer2.getText() != manswer[idkota][randomquestion]) {
                        Toast.makeText(getApplicationContext(), ALERT_WRONG_ANSWER, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            btnAnswer3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (btnAnswer3.getText() == manswer[idkota][randomquestion]) {
                        Toast.makeText(getApplicationContext(), "Right answer", Toast.LENGTH_SHORT).show();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Do something after 3 second
                                ShowDialog("Anda benar apakah lanjut?");
                            }
                        }, 1000);
                    } else if (btnAnswer3.getText() != manswer[idkota][randomquestion]) {
                        Toast.makeText(getApplicationContext(), ALERT_WRONG_ANSWER, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            isStart=0;
            ResumeAudio();
        }catch(Exception e){
            throw new RuntimeException(e.getMessage());
        }

    }

    private void init() {
        btnAnswer1 = (Button) findViewById(R.id.btnAnswer1);
        btnAnswer2 = (Button) findViewById(R.id.btnAnswer2);
        btnAnswer3 = (Button) findViewById(R.id.btnAnswer3);
        tvQuestion = (TextView) findViewById(R.id.tvQuestion);
        imgBackground = (ImageView)findViewById(R.id.imgBackground2);
    }

    private int RandomNumberQuestion(int City, int sizeQuestion) {
        Random random = new Random();
        return random.nextInt(sizeQuestion);
    }

    public void ShowDialog(String p_pesan_dialog) {
        UpDateScore();
        AlertDialog.Builder a_builder = new AlertDialog.Builder(PertanyaanActivity.this);
        a_builder.setMessage(p_pesan_dialog)
                .setCancelable(false)
                .setPositiveButton("Lanjutkan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(getApplicationContext(), TebakGambarActivity.class);
                        intent.putExtra("rute", String.valueOf(rute));
                        intent.putExtra("idkota", String.valueOf(idkota));
                        intent.putExtra("kota", kota);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        next();
                    }
                });
        AlertDialog alert = a_builder.create();
        //alert.setTitle(null);
        alert.show();
    }

    public void UpDateScore() {
        SpScore spscore = new SpScore(this);
        int lastScore = spscore.getScore();
        int updateScore = lastScore + 30;
        spscore.setScore(updateScore);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }


    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    public void next() {
       // StopAudio();
        Route route = new Route(rute, idkota);
        int nextroute = route.getNextroute();
        //Toast.makeText(getApplicationContext(),""+nextroute,Toast.LENGTH_SHORT).show();
        Intent audio = new Intent(getApplicationContext(), PlayAudio.class);


        if (nextroute == 99) {
            Intent intent = new Intent(PertanyaanActivity.this, ScoreActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(PertanyaanActivity.this, KotaActivity.class);
            intent.putExtra("rute", String.valueOf(rute));
            intent.putExtra("idkota", String.valueOf(nextroute));
            intent.putExtra("kota", kota);
            startActivity(intent);
            finish();
        }
    }



    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Data","onPause");
        if (isStart >= 0){
            PauseAudio();
        }

        isStart++;

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Data","onResume isStart="+isStart);
        if (isStart > 0){
            ResumeAudio();
        }
    }


    public void PauseAudio(){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","pause");
        sendBroadcast(in);
    }


    public void ResumeAudio(){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","resume");
        sendBroadcast(in);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

}
