package emcorp.studio.gameborneo;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import emcorp.studio.gameborneo.Library.Constant;
import emcorp.studio.gameborneo.Library.MusicReceiver;
import emcorp.studio.gameborneo.Library.PlayAudio;
import emcorp.studio.gameborneo.R;

public class KotaDetailActivity extends AppCompatActivity {
    ImageButton btnPrev;
    ImageView imgBackground, imgWisata;
    TextView tvInformasi;
    private int rute;
    private int idkota;
    private  int idinfo;
    private String kota;
    private String[] music;
    int isStart;
    int songId;

    MusicReceiver musicReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wisata);
        btnPrev = (ImageButton)findViewById(R.id.btnPrev);
        imgBackground = (ImageView)findViewById(R.id.imgBackground);
        imgWisata = (ImageView)findViewById(R.id.imgWisata);
        tvInformasi = (TextView)findViewById(R.id.tvInformasi);
        this.idkota = Integer.parseInt(getIntent().getStringExtra("idkota"));
        this.idinfo = Integer.parseInt(getIntent().getStringExtra("idinfo"));
        this.kota = getIntent().getStringExtra("kota");
        this.rute = Integer.parseInt(getIntent().getStringExtra("rute"));

        Constant constan = new Constant();
        this.music = constan.getMusic();
        String[][][] info_secondary = constan.getInfo_secondary();
        String[][] info_primary = constan.getInfo_primary();

        tvInformasi.setText(info_secondary[idkota][idinfo][1]);
        int backgroundImg = getResources().getIdentifier(info_primary[idkota][0], "drawable", getApplicationInfo().packageName);

        int wisataImg = getResources().getIdentifier(info_secondary[idkota][idinfo][0], "drawable", getApplicationInfo().packageName);

        imgBackground.setImageResource(backgroundImg);
        imgWisata.setImageResource(wisataImg);
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(KotaDetailActivity.this,KotaActivity.class);
//                intent.putExtra("rute",String.valueOf(rute));
//                intent.putExtra("idkota",String.valueOf(idkota));
//                startActivity(intent);
                finish();
            }
        });
        songId = getResources().getIdentifier(music[idkota], "raw", getApplicationInfo().packageName);
        isStart=0;
        ResumeAudio();
        //musicReceiver = new MusicReceiver();
        //IntentFilter intentFilter = new IntentFilter("emcorp.studio.gameborneo.musicreceiver");
        //registerReceiver(musicReceiver,intentFilter);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(KotaDetailActivity.this,KotaActivity.class);
//        intent.putExtra("rute",String.valueOf(rute));
//        intent.putExtra("idkota",String.valueOf(idkota));
//        startActivity(intent);
//        StopAudio();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Data","onPause");
        if (isStart >= 0){
            PauseAudio();
        }

        isStart++;

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Data","onResume isStart="+isStart);
        if (isStart > 0){
            ResumeAudio();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Data","onDestroy");
//        unregisterReceiver(musicReceiver);
    }

    public void PauseAudio(){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","pause");
        sendBroadcast(in);
    }


    public void ResumeAudio(){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","resume");
        sendBroadcast(in);
    }

    public void StopAudio(){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","stop");
        sendBroadcast(in);
    }

}
