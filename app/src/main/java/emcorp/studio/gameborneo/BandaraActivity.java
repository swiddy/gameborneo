package emcorp.studio.gameborneo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import emcorp.studio.gameborneo.Library.Constant;
import emcorp.studio.gameborneo.Library.SpScore;

public class BandaraActivity extends AppCompatActivity {

    private ImageButton btnBack;
    TextView tvRute1,tvRute2,tvRute3,tvRute4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initscore();
        setContentView(R.layout.activity_bandara);
        btnBack = (ImageButton)findViewById(R.id.btnBack);
        tvRute1 = (TextView)findViewById(R.id.tvRute1);
        tvRute2 = (TextView)findViewById(R.id.tvRute2);
        tvRute3 = (TextView)findViewById(R.id.tvRute3);
        tvRute4 = (TextView)findViewById(R.id.tvRute4);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BandaraActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        tvRute1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BandaraActivity.this, KotaActivity.class);
                intent.putExtra("rute",String.valueOf(Constant.ROUTE_1));
                intent.putExtra("idkota",String.valueOf(Constant.PONTIANAK));
                intent.putExtra("kota",Constant.Label_kota[0]);
                intent.putExtra("mulai",true);
                startActivity(intent);
                finish();
            }
        });
        tvRute2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BandaraActivity.this, KotaActivity.class);
                intent.putExtra("rute",String.valueOf(Constant.ROUTE_2));
                intent.putExtra("idkota",String.valueOf(Constant.PONTIANAK));
                intent.putExtra("kota",Constant.Label_kota[0]);
                intent.putExtra("mulai",true);
                startActivity(intent);
                finish();
            }
        });
        tvRute3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BandaraActivity.this, KotaActivity.class);
                intent.putExtra("rute",String.valueOf(Constant.ROUTE_3));
                intent.putExtra("idkota",String.valueOf(Constant.PONTIANAK));
                intent.putExtra("mulai",true);
                intent.putExtra("kota",Constant.Label_kota[0]);
                startActivity(intent);
                finish();
            }
        });
        tvRute4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BandaraActivity.this, KotaActivity.class);
                intent.putExtra("rute",String.valueOf(Constant.ROUTE_4));
                intent.putExtra("idkota",String.valueOf(Constant.PONTIANAK));
                intent.putExtra("mulai",true);
                intent.putExtra("kota",Constant.Label_kota[0]);
                startActivity(intent);
                finish();
            }
        });
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(BandaraActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    public void initscore(){
        SpScore spscore = new SpScore(this);
        spscore.setScore(0);
    }
}
