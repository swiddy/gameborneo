package emcorp.studio.gameborneo;

import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.Random;

import emcorp.studio.gameborneo.Library.Constant;
import emcorp.studio.gameborneo.Library.MusicReceiver;
import emcorp.studio.gameborneo.Library.PlayAudio;
import emcorp.studio.gameborneo.Library.SpScore;
import emcorp.studio.gameborneo.game.PertanyaanActivity;
import io.fabric.sdk.android.Fabric;

public class KotaActivity extends AppCompatActivity {
    private int rute;
    private int idkota;
    private String kota;

    public boolean isRun;

    private String[][] info_primary;
    private String[][][] info_secondary;
    private String[] music;
    ImageView imgBackground, img1, img2, img3, img4;
    TextView tvWisata, tvInformasi, tvKota;
    ImageButton btnNext;
    Constant constan = new Constant();
    MusicReceiver musicReceiver;
    int songId;
    int isStart;

    int[] ints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Data","onCreate");
        setContentView(R.layout.activity_city);

        Fabric.with(this,new Crashlytics());
        try {
            img1 = (ImageView) findViewById(R.id.img1);
            img2 = (ImageView) findViewById(R.id.img2);
            img3 = (ImageView) findViewById(R.id.img3);
            img4 = (ImageView) findViewById(R.id.img4);
            imgBackground = (ImageView) findViewById(R.id.imgBackground);
            tvWisata = (TextView) findViewById(R.id.tvWisata);
            tvKota = (TextView) findViewById(R.id.tvKota);
            tvInformasi = (TextView) findViewById(R.id.tvInformasi);
            btnNext = (ImageButton) findViewById(R.id.btnNext);


            this.idkota = Integer.parseInt(getIntent().getStringExtra("idkota"));
            //this.kota = getIntent().getStringExtra("kota");
            this.rute = Integer.parseInt(getIntent().getStringExtra("rute"));


            this.info_primary = constan.getInfo_primary();
            this.info_secondary = constan.getInfo_secondary();
            this.music = constan.getMusic();

            songId = getResources().getIdentifier(music[idkota], "raw", getApplicationInfo().packageName);
            if (getIntent().getBooleanExtra("mulai", false) == true) {
                musicReceiver = new MusicReceiver();
                IntentFilter intentFilter = new IntentFilter("emcorp.studio.gameborneo.musicreceiver");
                registerReceiver(musicReceiver, intentFilter);
                Log.d("Data boolean", "true");
                PlayAudio(songId);
            } else if (getIntent().getBooleanExtra("mulai", false) == false) {
                Log.d("Data boolean", "false");
                StopAudio();
//                IntentFilter intentFilter = new IntentFilter("emcorp.studio.gameborneo.musicreceiver");
//                musicReceiver = new MusicReceiver();
//               // registerReceiver(musicReceiver, intentFilter);
                PlayNext(songId);
            }


            String[] label = constan.getLabel_kota();
            this.kota = label[idkota];

            if(android.os.Build.VERSION.SDK_INT>Build.VERSION_CODES.M){
                Random random = new Random();
                ints = random.ints(0, info_secondary[idkota].length - 1).distinct().limit(3).toArray();
            }else {
                ints = randomnumber(info_secondary[idkota].length);
            }

            tvInformasi.setText(info_primary[idkota][2]);

            tvWisata.setText("Wisata dan Budaya " + kota);
            tvKota.setText(kota);

            int backgroundImg = getResources().getIdentifier(info_primary[idkota][0], "drawable", getApplicationInfo().packageName);
            imgBackground.setImageResource(backgroundImg);

            int v_img1 = getResources().getIdentifier(info_secondary[idkota][ints[0]][0], "drawable", getApplicationInfo().packageName);
            img1.setImageResource(v_img1);

            int v_img2 = getResources().getIdentifier(info_secondary[idkota][ints[1]][0], "drawable", getApplicationInfo().packageName);
            img2.setImageResource(v_img2);

            int v_img3 = getResources().getIdentifier(info_secondary[idkota][ints[2]][0], "drawable", getApplicationInfo().packageName);
            img3.setImageResource(v_img3);

            img1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GoTo(rute, idkota, ints[0]);
                }
            });

            img2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GoTo(rute, idkota, ints[1]);
                }
            });

            img3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GoTo(rute, idkota, ints[2]);
                }
            });

            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), PertanyaanActivity.class);
                    i.putExtra("rute", String.valueOf(rute));
                    i.putExtra("idkota", String.valueOf(idkota));
                    i.putExtra("kota", kota);
                    startActivity(i);
                    //finish();
                }
            });

            isStart = 0;
        }catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }


    }

    public void GoTo(int rute, int idkota, int idinfo){
        Intent intent = new Intent(getApplicationContext(),KotaDetailActivity.class);
        intent.putExtra("rute",String.valueOf(rute));
        intent.putExtra("idkota",String.valueOf(idkota));
        intent.putExtra("idinfo",String.valueOf(idinfo));
        startActivity(intent);
        //finish();
    }


    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Data","onPause");
        if (isStart >= 0){
            PauseAudio();
        }

        isStart++;

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Data","onResume isStart="+isStart);
        if (isStart > 0){
            ResumeAudio();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Data","onDestroy");
        if (getIntent().getBooleanExtra("mulai", false) == true){
            unregisterReceiver(musicReceiver);
        }
    }

    public void PlayAudio(int songId){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","play");
        sendBroadcast(in);

    }

    public void PlayNext(int songId){
        if (isRun == false) {
            Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
            in.putExtra("song", songId);
            in.putExtra("status", "next");
            sendBroadcast(in);
        }
    }

    public void PauseAudio(){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","pause");
        sendBroadcast(in);
    }


    public void ResumeAudio(){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","resume");
        sendBroadcast(in);
    }

    public void StopAudio(){
        Intent in = new Intent("emcorp.studio.gameborneo.musicreceiver");
        in.putExtra("song",songId);
        in.putExtra("status","stop");
        sendBroadcast(in);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Data","OnRestart");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Data","OnStart");
    }

    public int[] randomnumber(int p_length){
        int[] acak = new int[3];
        Random random = new Random();
        int last = p_length;
        for(int i=0;i<acak.length;i++){
            acak[i] = random.nextInt(last);
            last--;
        }
        return acak;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
